package com.epam.javalab.newsmanagement.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.javalab.newsmanagement.entity.Author;
import com.epam.javalab.newsmanagement.entity.Comment;
import com.epam.javalab.newsmanagement.entity.News;
import com.epam.javalab.newsmanagement.entity.NewsTO;
import com.epam.javalab.newsmanagement.entity.SearchCriteria;
import com.epam.javalab.newsmanagement.entity.Tag;
import com.epam.javalab.newsmanagement.exception.ResourceNotFoundException;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.IAuthorService;
import com.epam.javalab.newsmanagement.service.ICommentService;
import com.epam.javalab.newsmanagement.service.INewsManagementService;
import com.epam.javalab.newsmanagement.service.INewsService;
import com.epam.javalab.newsmanagement.service.ITagService;
import com.epam.javalab.newsmanagement.util.PageHelper;
import com.epam.javalab.newsmanagement.util.editor.LocalizedDateEditor;
import com.epam.javalab.newsmanagement.util.formatter.FormatHelper;

@Controller
public class NewsController {

	@Autowired
	private INewsManagementService newsManagementService;
	@Autowired
	private INewsService newsService;
	@Autowired
	private IAuthorService authorService;
	@Autowired
	private ITagService tagService;
	@Autowired
	private ICommentService commentService;
	@Autowired
	private FormatHelper formatHelper;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new LocalizedDateEditor(
				formatHelper));
	}

	/**
	 * Initializes paginate parameters. Finds list of news acordingly to search
	 * criteria in specified range.
	 * 
	 * @param page number of the page
	 * @param request
	 * @return model and <code>newsList<code> view
	 * @throws ServiceException
	 */
	@RequestMapping(value = { "/news", "/" })
	public ModelAndView showNews(
			@RequestParam(required = false, defaultValue = "1") String page,
			HttpServletRequest request) throws ServiceException {
		PageHelper pageHelper = new PageHelper();
		SearchCriteria sc = getSearchCriteriaFromSession(request);
		int totalRows = newsService.count(sc);
		pageHelper.calculate(Integer.parseInt(page), totalRows);

		List<NewsTO> newsList = newsManagementService.getLimitNews(
				pageHelper.getFirstRow(), pageHelper.getOffset(), sc);
		List<Author> authors = authorService.getAllAuthors();
		List<Tag> tags = tagService.getAllTags();

		ModelAndView modelAndView = new ModelAndView("newsList");
		modelAndView.addObject("authors", authors);
		modelAndView.addObject("tags", tags);
		modelAndView.addObject("newsTOList", newsList);
		modelAndView.addObject("searchCriteria", sc);
		modelAndView.addObject(pageHelper);
		return modelAndView;
	}

	/**
	 * Returns a search criteria from session. If there is no one then creates a
	 * new empty search criteria.
	 * 
	 * @param request
	 * @return the search criteria
	 */
	private SearchCriteria getSearchCriteriaFromSession(
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		SearchCriteria sc = (SearchCriteria) session
				.getAttribute("searchCriteria");
		if (sc == null) {
			sc = new SearchCriteria();
			session.setAttribute("searchCriteria", sc);
		}
		return sc;
	}

	/**
	 * Creates a new search criteria accordingly to passed search parameters.
	 * Adds search criteria attribute to session. Redirects to show news page.
	 * 
	 * @param request
	 * @return model and page to redirect
	 */
	@RequestMapping(value = { "/news/filter" })
	public ModelAndView filterNews(
			@ModelAttribute("searchCriteria") SearchCriteria sc,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("searchCriteria", sc);

		ModelAndView modelAndView = new ModelAndView("redirect:/news");
		return modelAndView;
	}

	/**
	 * Resets search filter creating a new empty search criteria and adds it to
	 * session. Redirects to show news page.
	 * 
	 * @param request
	 * @return model and page to redirect
	 */
	@RequestMapping(value = { "/news/resetFilter" }, method = RequestMethod.POST)
	public ModelAndView resetNewsFilter(HttpServletRequest request) {
		HttpSession session = request.getSession();
		SearchCriteria sc = new SearchCriteria();
		session.setAttribute("searchCriteria", sc);

		ModelAndView modelAndView = new ModelAndView("redirect:/news");
		return modelAndView;
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleResourceNotFoundException() {
		return "error404";
	}

	@RequestMapping(value = "/news/{id}", method = RequestMethod.GET)
	public String viewNews(@PathVariable Long id, HttpServletRequest request,
			Model model) throws NumberFormatException, ServiceException {

		NewsTO newsTO = newsManagementService.readNews(id);
		News news = newsTO.getNews();
		if (news == null) {
			throw new ResourceNotFoundException();
		}
		SearchCriteria sc = getSearchCriteriaFromSession(request);
		Long nextNewsId = newsService.findNextNewsId(sc, id);
		Long previousNewsId = newsService.findPreviousNewsId(sc, id);
		Comment comment = new Comment();
		comment.setNewsId(news.getId());

		model.addAttribute("nextNewsId", nextNewsId);
		model.addAttribute("previousNewsId", previousNewsId);
		model.addAttribute("newsTO", newsTO);
		if (!model.containsAttribute("comment")) {
			model.addAttribute("comment", comment);
		}

		return "viewNews";
	}

	/**
	 * Creates a new form for posting news. Initialize news creation form with
	 * all the tags and authors. Fill date field with current date.
	 * 
	 * @return model and view name to display
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/news/post", method = RequestMethod.GET)
	public ModelAndView initCreationForm() throws ServiceException {
		List<Author> authors = authorService.getAllAuthors();
		List<Tag> tags = tagService.getAllTags();
		Date creationDate = new Date();
		News news = new News();
		news.setCreationDate(creationDate);
		news.setModificationDate(creationDate);

		ModelAndView modelAndView = new ModelAndView("addNews");
		modelAndView.addObject("authors", authors);
		modelAndView.addObject("tags", tags);
		modelAndView.addObject("news", news);
		return modelAndView;
	}

	/**
	 * Collects all the information about news from the request. Creates a news
	 * with specified tags and author name. Redirects to news view that has just
	 * been created.
	 * 
	 * @param shortText brief description of the news
	 * @param fullText full content of the news
	 * @param title news title
	 * @param creationDate creation date of news
	 * @param request http servlet request to get request parameters
	 * @return model and view page to redirect
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/news/post", method = RequestMethod.POST)
	public ModelAndView processCreationForm(
			@Valid @ModelAttribute("news") News news, BindingResult result,
			HttpServletRequest request, @RequestParam("author") Long authorId,
			@RequestParam(value = "tags", required = false) Long[] tagIds)
			throws ServiceException {

		if (result.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("addNews");
			List<Author> authors = authorService.getAllAuthors();
			List<Tag> tags = tagService.getAllTags();
			modelAndView.addObject("authors", authors);
			modelAndView.addObject("tags", tags);
			return modelAndView;
		}

		news.setCreationDate(news.getModificationDate());

		Long id = newsManagementService.createNews(news, authorId,
				tagIds);

		ModelAndView modelAndView = new ModelAndView("redirect:/news/" + id);
		return modelAndView;
	}

	/**
	 * 
	 * @param id news id to update
	 * @return model and update news view
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/news/edit/{id}", method = RequestMethod.GET)
	public ModelAndView initUpdateNewsForm(@PathVariable Long id)
			throws ServiceException {
		List<Author> authors = authorService.getAllAuthors();
		List<Tag> tags = tagService.getAllTags();
		Date date = new Date();

		NewsTO newsTO = newsManagementService.readNews(id);
		if (newsTO.getNews() == null) {
			throw new ResourceNotFoundException();
		}

		ModelAndView modelAndView = new ModelAndView("editNews");
		modelAndView.addObject(date);
		modelAndView.addObject("authors", authors);
		modelAndView.addObject("tags", tags);
		modelAndView.addObject("newsTO", newsTO);
		modelAndView.addObject("news", newsTO.getNews());
		return modelAndView;
	}

	@RequestMapping(value = "/news/edit/{id}", method = RequestMethod.POST)
	public ModelAndView processUpdateNewsForm(@PathVariable Long id,
			@Valid @ModelAttribute("news") News updatedNews,
			BindingResult result, HttpServletRequest request,
			@RequestParam("author") Long authorId,
			@RequestParam(value = "tags", required = false) Long[] tagIds)
			throws ServiceException {

		if (result.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("editNews");
			List<Author> authors = authorService.getAllAuthors();
			List<Tag> tags = tagService.getAllTags();
			modelAndView.addObject("authors", authors);
			modelAndView.addObject("tags", tags);
			return modelAndView;
		}

		News news = newsService.read(id);

		if (news == null) {
			throw new ResourceNotFoundException();
		}

		news.setFullText(updatedNews.getFullText());
		news.setShortText(updatedNews.getShortText());
		news.setTitle(updatedNews.getTitle());
		news.setModificationDate(updatedNews.getModificationDate());

		newsManagementService.updateNews(news, authorId, tagIds);

		ModelAndView modelAndView = new ModelAndView("redirect:/news/" + id);
		return modelAndView;
	}

	/**
	 * Deletes list of news. Calculates total count of pages to display on the
	 * view. Redirects to show news page with parameter page. If current page
	 * above total count of pages then redirects to the last page.
	 * 
	 * @param request
	 * @param redirectAttributes attributes to pass with redirect page
	 * @return page name to redirect
	 * @throws NumberFormatException
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/news/delete", method = RequestMethod.POST)
	public String deleteNews(HttpServletRequest request,
			@RequestParam(value = "newsId", required = true) Long[] newsIds,
			RedirectAttributes redirectAttributes)
			throws NumberFormatException, ServiceException {

		newsService.deleteNewsList(newsIds);

		String page = (String) request.getParameter("page");
		if (page != null) {
			SearchCriteria sc = getSearchCriteriaFromSession(request);
			int totalRows = newsService.count(sc);
			PageHelper pageHelper = new PageHelper();
			pageHelper.calculate(Integer.parseInt(page), totalRows);
			if (Long.valueOf(page) > pageHelper.getTotalPages()) {
				page = String.valueOf(pageHelper.getTotalPages());
			}
			redirectAttributes.addAttribute("page", page);
		}

		return "redirect:";
	}

}
