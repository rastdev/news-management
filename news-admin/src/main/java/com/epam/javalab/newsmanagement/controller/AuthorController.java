package com.epam.javalab.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javalab.newsmanagement.entity.Author;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.IAuthorService;

@Controller
@RequestMapping(value = "authors")
public class AuthorController {

	@Autowired
	private IAuthorService authorService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView showAuthors() throws ServiceException {
		List<Author> authors = authorService.getAllAuthors();

		ModelAndView modelAndView = new ModelAndView("authorList");
		modelAndView.addObject("authors", authors);
		return modelAndView;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView updateAuthor(@RequestParam String id,
			@RequestParam String name) throws ServiceException {
		Author author = new Author();
		author.setId(Long.parseLong(id));
		author.setName(name);
		authorService.update(author);

		ModelAndView modelAndView = new ModelAndView("redirect:");
		return modelAndView;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView createAuthor(@RequestParam String name)
			throws ServiceException {
		Author author = new Author();
		author.setName(name);
		authorService.create(author);

		ModelAndView modelAndView = new ModelAndView("redirect:");
		return modelAndView;
	}

	@RequestMapping(value = "/expire", method = RequestMethod.POST)
	public ModelAndView expireAuthor(@RequestParam Long id)
			throws ServiceException {
		authorService.delete(id);
		
		ModelAndView modelAndView = new ModelAndView("redirect:");
		return modelAndView;
	}
}
