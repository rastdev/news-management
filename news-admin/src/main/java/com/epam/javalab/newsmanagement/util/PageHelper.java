package com.epam.javalab.newsmanagement.utils;

public class PageHelper {

	private int totalRows;
	private int firstRow;
	private int totalPages;
	private int pageRange;
	private int currentPage;
	private int rowsPerPage;
	private int offset;

	public void setOffset(int offset) {
		this.offset = offset;
	}

	private Integer[] pages;

	public PageHelper() {
		pageRange = 3;
		rowsPerPage = 6;
	}

	public int getPageRange() {
		return pageRange;
	}

	public void setPageRange(int pageRange) {
		this.pageRange = pageRange;
	}

	public int getTotalRows() {
		return totalRows;
	}

	public int getFirstRow() {
		return firstRow;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getOffset() {
		return offset;
	}

	public Integer[] getPages() {
		return pages;
	}

	public void calculate(int currentPage, int totalRows) {
		this.currentPage = currentPage;
		this.totalRows = totalRows;
		totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
		if (currentPage > totalPages) {
			currentPage = totalPages;
		} else if (currentPage < 1) {
			currentPage = 1;
		}
		firstRow = (currentPage - 1) * rowsPerPage + 1;
		offset = firstRow + rowsPerPage - 1;
		int pagesLength = Math.min(pageRange, totalPages);
		pages = new Integer[pagesLength];
		int firstPage = Math.min(Math.max(0, currentPage - (pageRange / 2)), totalPages - pagesLength);
		for (int i = 0; i < pagesLength; i++) {
			pages[i] = ++firstPage;
		}
	}
}
