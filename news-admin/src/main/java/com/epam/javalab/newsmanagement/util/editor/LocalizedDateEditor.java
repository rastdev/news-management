package com.epam.javalab.newsmanagement.util.editor;

import java.text.SimpleDateFormat;

import org.springframework.beans.propertyeditors.CustomDateEditor;

import com.epam.javalab.newsmanagement.util.formatter.FormatHelper;

public class LocalizedDateEditor extends CustomDateEditor {

    public LocalizedDateEditor(FormatHelper formatHelper) {
        super(new SimpleDateFormat(formatHelper.getDefaultDateFormat()), true);
    }
}
