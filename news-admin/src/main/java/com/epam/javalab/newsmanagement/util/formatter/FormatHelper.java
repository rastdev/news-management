package com.epam.javalab.newsmanagement.util.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public class FormatHelper {

	private final static String DEFAULT_DATE_PATTERN_KEY = "date.pattern";
	
	@Autowired
	private MessageSource messageSource;
	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	/**
	 * Recieves localized date pattern from message source
	 * 
	 * @return date pattern depending on current locale
	 */
	public String getDefaultDateFormat() {
		Locale locale = LocaleContextHolder.getLocale();
		String datePattern = messageSource.getMessage(DEFAULT_DATE_PATTERN_KEY, null, locale);
		return datePattern;
	}
}
