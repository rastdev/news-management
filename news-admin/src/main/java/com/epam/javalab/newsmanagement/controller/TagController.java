package com.epam.javalab.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javalab.newsmanagement.entity.Tag;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.ITagService;

@Controller
@RequestMapping(value = "tags")
public class TagController {

	@Autowired
	private ITagService tagService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView showTags() throws ServiceException {
		List<Tag> tags = tagService.getAllTags();

		ModelAndView modelAndView = new ModelAndView("tagList");
		modelAndView.addObject("tags", tags);
		return modelAndView;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView updateTag(@RequestParam String id,
			@RequestParam String name) throws ServiceException {
		Tag tag = new Tag();
		tag.setId(Long.parseLong(id));
		tag.setName(name);
		tagService.update(tag);

		ModelAndView modelAndView = new ModelAndView("redirect:");
		return modelAndView;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView createTag(@RequestParam String name)
			throws ServiceException {
		Tag tag = new Tag();
		tag.setName(name);
		tagService.create(tag);

		ModelAndView modelAndView = new ModelAndView("redirect:");
		return modelAndView;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ModelAndView deleteTag(@RequestParam Long id)
			throws ServiceException {
		tagService.delete(id);
		
		ModelAndView modelAndView = new ModelAndView("redirect:");
		return modelAndView;
	}
}