<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<form action="j_spring_security_check" method="post" class="form">
	<fieldset>
		<legend>Login Form</legend>
		<label for="j_username">Username</label> <input id="j_username"
			id="uname" name="j_username" size="20" maxlength="50" type="text" />
		<br /> <label for="j_password">Password</label> <input
			id="j_password" id="pword" name="j_password" size="20" maxlength="50"
			type="password" /> <br /> <label for="_spring_security_remember_me">Remember
			Me?</label> <input id="_spring_security_remember_me"
			name="_spring_security_remember_me" type="checkbox" value="true"
			class="checkbox" /><br /> <input type="submit" value="Login"
			name="submit" class="buttonsubmit" />
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>.
	</fieldset>
</form>