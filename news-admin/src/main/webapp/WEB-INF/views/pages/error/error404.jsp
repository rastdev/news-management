<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="error-page-wrap">
	<article class="error-page gradient">
		<hgroup>
			<h1>404</h1>
			<h2><spring:message code="label.not_found"/></h2>
		</hgroup>
		<a href="" title="Back to site" class="error-back">back</a>
	</article>
</div>