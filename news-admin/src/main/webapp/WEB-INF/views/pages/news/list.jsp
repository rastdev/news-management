<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id="buttons">

	<form method="post">
		<table align="center">
			<tr>
				<td><select>
						<c:forEach var="author" items="${authors}">
							<option>${author.name}</option>
						</c:forEach>
				</select></td>

				<td><select id="s5" multiple="multiple" name="dropdown">
						<option>All</option>

						<c:forEach var="tag" items="${tags}">
							<option>${tag.name}</option>
						</c:forEach>
				</select></td>
				<td><input type="submit"
					value="<spring:message code="button.filter"/>" /></td>
			</tr>
		</table>
	</form>
</div>

<c:forEach var="newsTO" items="${newsTOList}">
	<c:set var="author" value="${newsTO.author}" />
	<c:set var="news" value="${newsTO.news}" />
	<c:set var="tags" value="${newsTO.tags}" />
	<c:set var="comments" value="${newsTO.comments}" />
	<div class="entry">
		<div class="shortText">${news.title}</div>
		<div class="author">(by ${author.name})</div>
		<div class="date">
			<fmt:formatDate value="${news.creationDate}" />
		</div>
		<div class="fullText">${news.shortText}</div>
		<div>
			<div class="view">
				<a href="${pageContext.servletContext.contextPath}/viewNews/${news.id}"><spring:message
						code="label.view" /></a>
			</div>
			<div class="comments">
				<spring:message code="label.comments" />
				(${fn:length(comments)})
			</div>
			<div class="tags">
				<c:forEach var="tag" items="${tags}" varStatus="status">
				${tag.name}
				<c:if test="${!status.last}">,</c:if>
				</c:forEach>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</c:forEach>
<div class="pagination">
	<ul class="paginate light">
		<li class="prev">Prev</li>
		<c:forEach var="i" items="${pageHelper.pages}">
			<c:choose>
				<c:when test="${i eq pageHelper.currentPage}">
					<li class="current">${i}</li>
				</c:when>
				<c:otherwise>
					<li><a href="listNews?page=${i}">${i}</a></li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<li class="next">Next</li>
	</ul>
</div>