<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="author" value="${newsTO.author}" />
<c:set var="news" value="${newsTO.news}" />
<c:set var="tags" value="${newsTO.tags}" />
<c:set var="comments" value="${newsTO.comments}" />
<a href="#" class="back-button"><span><spring:message
			code="button.back" /></span></a>
<div class="clearfix"></div>
<div class="news-wrapper">
	<div class="shortText">${news.title}</div>
	<div class="author">(by ${author.name})</div>
	<div class="news-date">
		<fmt:formatDate value="${news.creationDate}" />
	</div>
	<div class="fullText">${news.fullText}</div>
	<div id="comment-container">
		<c:forEach var="comment" items="${comments}">
			<div class="comment">
				<div class="comment-date"><fmt:formatDate value="${comment.creationDate}"/></div>
				<div class="message">
					<span>${comment.commentText}</span>
				</div>
			</div>
		</c:forEach>
		<c:if test="${empty comments}">
			<div id="absent-comments">Комментарии отсутствуют</div>
		</c:if>
	</div>
	<form action="/news-user/addComment" method="post" class="basic-grey" id="target">

		<label> <span>Message :</span> <textarea id="message"
				name="commentText" placeholder="Your Message to Us"></textarea>
		</label> <label> <span>&nbsp;</span> <input type="submit"
			class="submit-button"
			value="<spring:message code="button.post_comment"/>" />
		</label>
		<input type="hidden" name="newsId" value="${news.id}"/> 
		<div class="clearfix"></div>
	</form>
</div>
<a class="button back" href="#"><spring:message
		code="button.previous" /></a>
<a class="button next" href="#"><spring:message code="button.next" /></a>