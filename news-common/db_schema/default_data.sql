--------------------------------------------------------
--  File created - Thursday-March-26-2015   
--------------------------------------------------------
REM INSERTING into JAVA.AUTHOR
SET DEFINE OFF;
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (8,'Виталий Петров');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (22,'Александр Черн');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (23,'Полина Шумицкая');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (41,'Галкин Сергей');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (43,'Ивакин Игорь');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (42,'Трофимов Алекс');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (44,'Манько Тамара');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (1,'Денис Логунович');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (2,'Андрей Гомыляев');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (3,'Виталий Петров');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (4,'Андрей Журов');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (5,'Алексей Носов ');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (6,'Артур Боровой ');
Insert into JAVA.AUTHOR (AUTHOR_ID,NAME) values (7,'Николай Козлов');
REM INSERTING into JAVA.COMMENTS
SET DEFINE OFF;
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Одним больше одним меньше',to_timestamp('17-MAR-15 06.11.48.027000000 AM','DD-MON-RR HH.MI.SSXFF AM'),9,13);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Директор шел к успеху:)))',to_timestamp('16-MAR-15 06.20.07.872000000 AM','DD-MON-RR HH.MI.SSXFF AM'),22,22);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Монастыри - это не спасение!',to_timestamp('17-MAR-15 06.24.29.687000000 AM','DD-MON-RR HH.MI.SSXFF AM'),23,23);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Есть выбор. Это главное!',to_timestamp('13-MAR-15 06.34.11.070000000 AM','DD-MON-RR HH.MI.SSXFF AM'),24,24);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Что-то и сходить некуда:(',to_timestamp('14-MAR-15 06.38.06.912000000 AM','DD-MON-RR HH.MI.SSXFF AM'),24,25);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Молодец. Полностью поддерживаю такие инициативы.',to_timestamp('17-MAR-15 06.41.19.255000000 AM','DD-MON-RR HH.MI.SSXFF AM'),25,26);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Качаю',to_timestamp('18-MAR-15 06.41.39.212000000 AM','DD-MON-RR HH.MI.SSXFF AM'),25,27);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Что все?',to_timestamp('16-MAR-15 06.46.58.921000000 AM','DD-MON-RR HH.MI.SSXFF AM'),26,28);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Люблю Онлайнер за фото!',to_timestamp('17-MAR-15 06.47.22.376000000 AM','DD-MON-RR HH.MI.SSXFF AM'),26,29);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('получайте ответку',to_timestamp('11-MAR-15 10.23.54.281000000 AM','DD-MON-RR HH.MI.SSXFF AM'),2,1);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Сколько у меня в регистраторе таких случаев!',to_timestamp('11-MAR-15 12.25.07.278000000 PM','DD-MON-RR HH.MI.SSXFF AM'),2,2);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Никогда не понимал мотоциклистов!',to_timestamp('11-MAR-15 03.48.42.450000000 PM','DD-MON-RR HH.MI.SSXFF AM'),4,3);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('сезон открыт! минус 1',to_timestamp('11-MAR-15 04.49.06.278000000 PM','DD-MON-RR HH.MI.SSXFF AM'),4,4);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Знаков нет? Имеет право.',to_timestamp('13-MAR-15 10.51.12.618000000 AM','DD-MON-RR HH.MI.SSXFF AM'),3,7);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Где недремлющее ГАИ?',to_timestamp('13-MAR-15 11.51.56.586000000 AM','DD-MON-RR HH.MI.SSXFF AM'),3,8);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Трамваи нужно уважать- там девушки за рулем!',to_timestamp('12-MAR-15 10.57.50.290000000 AM','DD-MON-RR HH.MI.SSXFF AM'),5,9);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('да мне кажется он сейчас у каждого второго школьника.',to_timestamp('17-MAR-15 11.00.50.084000000 AM','DD-MON-RR HH.MI.SSXFF AM'),6,10);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('Все, настаёт еврец!',to_timestamp('17-MAR-15 11.03.48.570000000 AM','DD-MON-RR HH.MI.SSXFF AM'),7,11);
Insert into JAVA.COMMENTS (COMMENT_TEXT,CREATION_DATE,NEWS_ID,COMMENT_ID) values ('На фоне нашего у Польши лишь мелкие неприятности :)',to_timestamp('11-MAR-15 11.19.31.763000000 AM','DD-MON-RR HH.MI.SSXFF AM'),8,12);
REM INSERTING into JAVA.NEWS
SET DEFINE OFF;
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (9,'В Минске на проспекте снесли пешеходный светофор','ДТП случилось сегодня около 15:00. Как рассказала корреспонденту Onliner.by старший инспектор по агитации и пропаганде ГАИ Московского района Любовь Трепашко, автомобили двигались по проспекту Жукова в направлении улицы Аэродромной в крайней левой полосе. Водители не сумели вовремя сориентироваться и затормозить. В итоге образовался «паровозик». Также сбит пешеходный светофор.','Cнесли светофор',to_timestamp('17-MAR-15 06.06.16.366000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('17-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (22,'Производство самогона на берегу Ислочи','рупное производство самогона обнаружили сотрудники милиции в Налибокской пуще — 7 металлических емкостей, в которых было больше тонны браги. Производство скрывалось самогонщиками в глухом месте, куда не добираются даже местные охотники и рыбаки.— Судя по следам, самогонщики приезжали к мини-заводу на лодке, для приготовления спиртного вода забиралась из реки Ислочи, отходы «производства» выливались туда же, — рассказали в УВД Миноблисполкома.','Производство',to_timestamp('16-MAR-15 06.18.50.572000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (23,'Репортаж из вымирающей деревни','Деревенька Барань затерялась где-то в лесной глуши Борисовского района. Путь сюда непростой: дорога, судя по всему, не ремонтировалась очень давно и одним своим видом разрушает миф о белорусском автомобильном комфорте. Езды из райцентра до Барани от силы полчаса, которые рискуют превратиться для неопытного водителя в целое приключение. Именно здесь расположен Свято-Ксениевский женский монастырь — место, благодаря которому вымирающая деревня известна во всей стране.Заприметить белые церковные стены можно уже на въезде в деревню. Монастырь расположился на возвышенности в лесу, рядом с сельским кладбищем. Вход в монашескую обитель — через массивные ворота. Здесь находятся главный храм в честь Святой Блаженной Ксении Петербургской, каменная церковь в честь Георгия Победоносца и корпус монастыря.','женский монаст',to_timestamp('16-MAR-15 06.23.00.810000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (24,'Куда сходить в выходные','В выходные можно научиться танцевать, как Шакира, убедиться в том, что репер Guf все еще жив, посмотреть нашумевший российский фильм «Дурак» и побывать на масштабном ледовом шоу Ильи Авербуха. Эти и другие события в афише Onliner.by.В «Автокинотеатре» идет «младший брат» номинированного на «Оскар» «Левиафана» — фильм Юрия Быкова «Дурак». Это социальная драма о жизни в российской глубинке: молодой сантехник пытается спасти жизни 800 жильцов аварийного дома и вступает в неравный бой с городской администрацией. Многие считают, что в эмоциональном плане лента даже сильнее нашумевшего «Левиафана».','Выбор Onliner.by',to_timestamp('13-MAR-15 06.27.07.206000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('14-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (25,'Минский дизайнер создал шрифт','Написать письмо почерком Янки Купалы теперь можно при помощи компьютера. Дизайнер-фрилансер из Минска Павел Минкевич создал шрифт Рiasniar на основе рукописей знаменитого белорусского поэта.— Хочется сделать белорусскую культуру ближе к современной, — говорит Павел. — Пришла в голову такая идея, я собрал команду единомышленников, и за пару месяцев мы все сделали.','Шрифт',to_timestamp('17-MAR-15 06.40.00.367000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('17-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (26,'Белорусский режиссер снял боевик','Белорусский режиссер Иван Маслюков и его съемочная группа уложились в весьма скромную для полнометражного кино сумму — $25 000 — и весьма короткий срок — семь месяцев. Именно столько понадобилось команде энтузиастов, чтобы отснять все сцены, закончить монтаж и показать приключенческий фильм «Схватка» широкой публике в кинотеатре «Беларусь». Теперь картина появилась в свободном доступе в сети. О том, почему он отказался от идеи заработать на своем кино, режиссер рассказал Onliner.by.','Боевик',to_timestamp('16-MAR-15 06.45.04.640000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('16-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (1,'Mercedes готовит рестайлинг роскошного родстера SL','В небольшом живописном городке Зиндельфинген, что недалеко от Штутгарта (Германия), шпионские фотографы CarAdvice запечатлели Mercedes SL, чей кузов покрыт камуфляжной пленкой. По старой немецкой примете это означает, что роскошный родстер вскоре получит рестайлинг и его жизненный цикл приближается к половине. Вся передняя часть SL замаскирована. Родстер оборудуют видоизмененной решеткой радиатора, иным бампером, доработанной головной оптикой и слегка переделанным капотом. Двери и стойки автомобиля останутся неизменными, как и жесткая крыша, убирающаяся в багажное отделение Mercedes SL.Сзади тоже ожидаются изменения. Так, модель получит новый бампер, более массивный диффузор и, возможно, иные патрубки выпускной системы. Нынешнее поколение Mercedes SL выпускается с 2012 года. Машина является одним из самых дорогих в мире родстеров.','Родстер SL',to_timestamp('12-MAR-15 10.09.31.490000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('15-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (2,'Ролики про мусорящих милиционеров','Резонанс вызвало сообщение о штрафе размером в одну базовую величину, который получил водитель Volkswagen Transporter за выброшенный в окно автомобиля окурок. «Сотрудники ГАИ реагируют не только на грубые нарушения Правил дорожного движения, — прокомментировали в Госавтоинспекции. — К ответственности привлекаются и водители, которые лишь на первый взгляд не совершают ничего предосудительного». Вскоре в ответ на публикацию в сети появилось немало роликов, в которых нарушителями являются представители правоохранительных органов.
Все началось с данного видео.Водителя наказали, руководствуясь пунктом 1 статьи 18.9 КоАП, которая предусматривает за выбрасывание мусора или иных предметов из транспортного средства наложение штрафа в размере от одной до двух базовых величин.
Вскоре началась ответная реакция.
','Штраф за окурок',to_timestamp('11-MAR-15 10.14.52.237000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('12-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (3,'В Минске водитель припарковался на встречной и ушел','Видео со своего регистратора прислал в редакцию Onliner.by читатель Алексей. Вчера он столкнулся с необычной ситуацией на дороге. Средь бела дня прямо перед его автомобилем на встречную выехал водитель Mercedes, припарковался у обочины и как ни в чем не бывало ушел, поставив машину на сигнализацию.«Это 2-й Велосипедный переулок. Вчера днем еду, никого не трогаю. И тут через сплошную передо мной прямо в лоб паркуется этот Mercedes. На мой сигнал водитель сделал удивленное лицо, мол, что не так? В такие минуты жалею, что я не инспектор или хотя бы не милиционер, чтобы законно разобраться вот с таким», — рассказал Алексей.Времени вызывать инспекторов у нашего читателя на тот момент не было. Но он не намерен оставлять этот инцидент без внимания. «Завтра пойду в ГАИ района, если видео будет основанием для привлечения к ответственности, то напишу заявление. Таким не место на дороге. Хотя не исключено, что они аналогично захотят привлечь и меня, ведь это чудо вынудило и меня пересечь сплошную», — отметил наш читатель.','Припарковался',to_timestamp('13-MAR-15 10.41.40.758000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('14-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (4,'Водитель не пропустил мотоциклиста.','ДТП случилось вчера около 15:20. Как сообщили в УГАИ МВД, 58-летний водитель, гражданин России (имеет вид на жительство в Беларуси), не имея права управления, двигался за рулем Volkswagen Passat по улице Первомайской в Пинске. Поворачивая налево, мужчина не уступил дорогу двигавшемуся во встречном направлении мотоциклисту на Suzuki. В результате ДТП мотоциклиста 1984 года рождения в тяжелом состоянии госпитализировали. Он скончался в больнице.За управление мототранспортом и перевозку на нем пассажиров без или с незастегнутыми мотошлемами взыскивается штраф до 1 базовой величины (в настоящее время она составляет 180 тыс.) Повторное такое нарушение в течение года повлечет увеличение ответственности — от 2 до 5 б. в. (от 360 тыс. до 900 тыс.).','Конфликт левого',to_timestamp('11-MAR-15 10.45.57.145000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('12-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (5,'Аварии на рельсах в Минске','Самый несправедливо обиженный транспорт в столице — это трамвай. Ну судите сами: кто-то неудачно припарковался, перекрыв проезд по рельсам, — вагончики стоят; на путях столкнулись легковушки — опять непредвиденная остановка. Периодически автомобилисты поднимают волну: уберите вообще трамвай из города! Но этот транспорт, например, заменяет метро для Зеленого Луга и связывает отдаленные микрорайоны с центром. Каждый вагон перевозит столько же пассажиров, сколько и автобус-гармошка. А еще водители трамваев одни из самых аккуратных: в 181 прошлогодней аварии с их участием они были виновны лишь в четырех! Рассмотрим серию роликов, сделанных видеорегистраторами, чтобы разобраться в причинах типичных ДТП.Вот запись самого типичного столкновения на Логойском тракте. Трамвай разгоняется на перегоне. Но на перекрестке с ул. Калиновского перед ним появляется Volkswagen, который ме-е-едленно поворачивает направо. Обычно этот маневр не сопряжен с большими сложностями — нужно только пропустить пешеходов.','Не объедет!',to_timestamp('12-MAR-15 10.54.12.904000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('12-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (6,'Дома у минского школьника нашли спайс','Были времена, когда школьники хранили в комнатах журналы с наклейками Panini, а теперь в «детских» нередко обнаруживают спайс. Очередной эпизод зафиксирован в Центральном районе.
— Поведение ученика одной из минских школ было подозрительным, и администрация обратилась в инспекцию по делам несовершеннолетних, — рассказала пресс-офицер Центрального РУВД Татьяна Обозная. — Сотрудники наркоконтроля провели проверку. В присутствии мамы девятиклассника и его самого в комнате нашли пакетик с курительной смесью. Ученик пояснил, что впервые попробовал миксы два года назад и с тех пор употребляет их регулярно.','Начал в 7-м класс',to_timestamp('17-MAR-15 10.59.34.785000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('17-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (7,'евро наконец стал дорожать','Очередные торги прошли сегодня на бирже. По их итогам американский доллар подешевел на 20 рублей и продавался по 15 100 белорусских. Евро взял себя в руки и вырос в цене.','Свежие торги',to_timestamp('17-MAR-15 11.02.15.304000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('17-MAR-15','DD-MON-RR'));
Insert into JAVA.NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (8,'Сравниваем цены на продукты','Две страны-соседки связаны общей историей, границей и маяком шопинга — Белостоком. В 1990-х годах поляки приезжали в Беларусь за железными ведрами и шерстяными штанами, в 2000-х процесс получил обратный ход — уже мы вывозим через Кузницу и Бобровники туалетную бумагу и телевизоры. Формально наша «похожесть» отражается и в экономике. Белорусский рубль рухнул, но девальвируется и польский злотый. О том, ощущается ли кризис в Белостоке и окрестностях, наш сегодняшний материал.
Суббота, раннее утро, Atrium Biala. Парковка забита машинами с белорусскими номерами. В магазинах слышна родная речь. Все как обычно, а тут еще цифры на табло обменника оптимистично подмигивают: 3,74 злотого за доллар.','Польская деваль',to_timestamp('11-MAR-15 11.17.42.079000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('11-MAR-15','DD-MON-RR'));
REM INSERTING into JAVA.NEWS_AUTHOR
SET DEFINE OFF;
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (3,3,3);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (4,4,4);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (5,5,5);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (6,6,6);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (7,7,7);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (8,8,8);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (1,1,1);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (2,2,2);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (9,22,9);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (22,23,10);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (23,41,11);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (24,42,12);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (25,43,13);
Insert into JAVA.NEWS_AUTHOR (NEWS_ID,AUTHOR_ID,NEWS_AUTHOR_ID) values (26,44,14);
REM INSERTING into JAVA.NEWS_TAG
SET DEFINE OFF;
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (15,9,4);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (22,9,6);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (23,23,21);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (24,23,22);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (25,24,4);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (26,24,23);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (27,24,24);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (28,24,25);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (29,25,26);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (30,25,27);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (31,26,24);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (32,26,28);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (1,8,1);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (2,8,2);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (3,8,3);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (4,3,4);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (5,3,5);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (6,3,6);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (7,1,7);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (8,1,8);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (9,4,9);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (10,4,10);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (11,4,11);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (12,2,12);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (13,2,13);
Insert into JAVA.NEWS_TAG (NEWS_TAG_ID,NEWS_ID,TAG_ID) values (14,2,6);
REM INSERTING into JAVA.TAG
SET DEFINE OFF;
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (21,'Деревня');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (22,'Монастырь');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (23,'Вечеринки');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (24,'Кино');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (25,'Концерты');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (26,'Шрифт');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (27,'Литература');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (28,'Экстрим');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (1,'Цены');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (2,'Белосток');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (3,'Шоппинг');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (4,'Минск');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (5,'Парковка');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (6,'Видео');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (7,'Mersedes SL');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (8,'Mersedes');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (9,'Аварии');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (10,'Брестская обл.');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (11,'Мото');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (12,'Штрафы');
Insert into JAVA.TAG (TAG_ID,TAG_NAME) values (13,'Гродненская обл.');
