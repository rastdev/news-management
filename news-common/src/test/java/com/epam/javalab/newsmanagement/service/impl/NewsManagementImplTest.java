package com.epam.javalab.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.javalab.newsmanagement.entity.News;
import com.epam.javalab.newsmanagement.service.IAuthorService;
import com.epam.javalab.newsmanagement.service.ICommentService;
import com.epam.javalab.newsmanagement.service.INewsService;
import com.epam.javalab.newsmanagement.service.ITagService;
import com.epam.javalab.newsmanagement.service.impl.NewsManagementServiceImpl;

public class NewsManagementImplTest {
	
	@Mock
	private IAuthorService authorService;
	@Mock
	private ICommentService commentService;
	@Mock
	private ITagService tagService;
	@Mock
	private INewsService newsService;
	@InjectMocks
	private NewsManagementServiceImpl newsManagementService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void shouldCreateNews() throws Exception {
		News news = new News();
		news.setTitle("����������� �����������");
		news.setShortText("� ������ �� ������� ����������� ����� �����������");
		news.setFullText("����������� �����������, ������������������ � ������� "
				+ "�� �����, ��������� � ���������� ������ ������, �� ����� "
				+ "������� 2015 ���� ��������� 4 ���. �������, ��� � 2,1 ���� "
				+ "������, ��� �� ����� ������� 2014 ����, � �� 27,5% ������, ��� �� "
				+ "����� ������ 2015 ����, �������� � ������� �������������� ���������� �������.");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		Long authorId = 1L;
		Long[] tagIds = {1L, 2L};
		when(newsService.create(news)).thenReturn(1L);
		Long newsId = newsManagementService.createNews(news, authorId, tagIds);
		assertEquals(Long.valueOf(1L), newsId);
		InOrder inOrder = inOrder(newsService);
		inOrder.verify(newsService).create(news);
		inOrder.verify(newsService).attachNewsAuthor(newsId, authorId);
		inOrder.verify(newsService).attachNewsTags(newsId, tagIds);
	}
	
}
