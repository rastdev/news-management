package com.epam.javalab.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.javalab.newsmanagement.dao.ITagDao;
import com.epam.javalab.newsmanagement.entity.Tag;
import com.epam.javalab.newsmanagement.service.impl.TagServiceImpl;

public class TagServiceImplTest {

	@Mock
	private ITagDao tagDao;
	@InjectMocks
	private TagServiceImpl tagService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void shouldCreateTag() throws Exception {
		Tag tag = new Tag();
		tag.setName("Памятники истории");
		when(tagDao.create(tag)).thenReturn(1L);
		Long result = tagService.create(tag);
		assertEquals(Long.valueOf(1L), result);
		verify(tagDao).create(tag);
	}
	
	@Test
	public void shouldDeleteTag() throws Exception {
		doNothing().when(tagDao).delete(1L);
		tagService.delete(1L);
		verify(tagDao).delete(1L);
	}
	
	@Test
	public void shouldUpdateTag() throws Exception {
		Tag tag = new Tag();
		tag.setId(1L);
		tag.setName("Памятники истории");
		doNothing().when(tagDao).update(tag);
		tagService.update(tag);
		verify(tagDao).update(tag);;
	}
	
	@Test
	public void shouldReadTag() throws Exception {
		Tag tag = new Tag();
		tag.setId(1L);
		tag.setName("Памятники истории");
		when(tagDao.read(1L)).thenReturn(tag);
		Tag result = tagService.read(1L);
		assertEquals(tag, result);
		verify(tagDao).read(1L);
	}
	
	@Test
	public void shouldGetAllNewsTags() throws Exception {
		List<Tag> tags = new ArrayList<Tag>();
		Tag tag = new Tag();
		Long newsId = 1L;
		tag.setId(1L);
		tag.setName("Памятники истории");
		when(tagDao.getAllNewsTags(newsId)).thenReturn(tags);
		List<Tag> result = tagService.getAllNewsTags(newsId);
		assertEquals(tags, result);
		verify(tagDao).getAllNewsTags(newsId);
	}
}
