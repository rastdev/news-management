package com.epam.javalab.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.javalab.newsmanagement.dao.ICommentDao;
import com.epam.javalab.newsmanagement.entity.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "/dbunit/CommentTestDataSet.xml")
public class CommentDaoJDBCImplTest {

	@Autowired
	private ICommentDao commentDao;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");;
	
	@Test
	public void testAddComment() throws Exception {
		Comment comment = new Comment();
		comment.setCommentText("��� ���?");
		comment.setNewsId(1L);
		comment.setCreationDate(new Date());
		Long commentId = commentDao.create(comment);
		Comment result = commentDao.read(commentId);
		assertEquals(comment.getCommentText(), result.getCommentText());
		assertEquals(comment.getCreationDate(), result.getCreationDate());
		assertEquals(comment.getNewsId(), result.getNewsId());
	}
	
	@Test
	public void testReadComment() throws Exception {
		Comment comment = commentDao.read(1L);
		assertEquals(new Long(1L), comment.getId());
		assertEquals("��������� �������", comment.getCommentText());
		assertEquals(sdf.parse("2015-01-18 12:45:55 PM"), comment.getCreationDate());
		assertEquals(new Long(1L), comment.getNewsId());
	}
	
	@Test
	public void testUpdateComment() throws Exception {
		Comment comment = commentDao.read(1L);
		comment.setCommentText("����� ������ ����� ������");
		commentDao.update(comment);
		Comment result = commentDao.read(1L);
		assertEquals(comment.getCommentText(), result.getCommentText());
	}
	
	@Test
	public void testDeleteComment() throws Exception {
		Long commentId = 1L;
		assertEquals(1, commentDao.getAllNewsComments(1L).size());
		commentDao.delete(commentId);
		assertTrue(commentDao.getAllNewsComments(1L).isEmpty());
	}
	
	@Test
	public void testGetAllNewsComments() throws Exception {
		List<Comment> comments = 
				commentDao.getAllNewsComments(1L);
		assertEquals(1, comments.size());
		Comment comment = comments.get(0);
		assertEquals(new Long(1L), comment.getId());
		assertEquals("��������� �������", comment.getCommentText());
		assertEquals(new Long(1L), comment.getNewsId());
		assertEquals(sdf.parse("2015-01-18 12:45:55 PM"), comment.getCreationDate());
	}
}
	