package com.epam.javalab.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.javalab.newsmanagement.dao.IAuthorDao;
import com.epam.javalab.newsmanagement.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "/dbunit/AuthorTestDataSet.xml")
public class AuthorDaoJDBCImplTest {

	@Autowired
	private IAuthorDao authorDao;
	
	@Test
	public void testCreateAuthor() throws Exception {
		Author author = new Author();
		author.setName("������ �����");
		Long authorId = authorDao.create(author);
		Author result = authorDao.read(authorId);
		assertEquals(new Long(authorId), result.getId());
		assertEquals(author.getName(), result.getName());
		assertEquals(4, authorDao.getAllAuthors().size());
	}
	
	@Test
	public void testReadAuthor() throws Exception {
		Author author = authorDao.read(232L);
		assertEquals(new Long(232L), author.getId());
		assertEquals("������� ���", author.getName());
	}
	
	@Test
	public void testUpdateAuthor() throws Exception {
		Author author = authorDao.read(234L);
		assertEquals("������ �����", author.getName());
		author.setName("������� �������");
		authorDao.update(author);
		Author result = authorDao.read(234L);
		assertEquals(author.getName(), result.getName());
	}
	
	@Test
	public void testDeleteAuthor() throws Exception {
		Long authorId = 232L;
		authorDao.delete(authorId);
		assertEquals(2, authorDao.getAllAuthors().size());
	}

	@Test
	public void testGetAllAuthors() throws Exception {
		List<Author> authors = authorDao.getAllAuthors();
		assertFalse(authors.isEmpty());
		assertEquals(3, authors.size());
	}
	
}
