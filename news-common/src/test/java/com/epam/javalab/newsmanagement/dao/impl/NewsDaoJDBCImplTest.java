package com.epam.javalab.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.javalab.newsmanagement.dao.INewsDao;
import com.epam.javalab.newsmanagement.entity.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "/dbunit/NewsTestDataSet.xml")
public class NewsDaoJDBCImplTest {

	@Autowired
	private INewsDao newsDao;
	private Calendar calendar;
	private SimpleDateFormat sdfTimeStamp = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
	private SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
	
	{
		calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}
	
	@Test
	public void testAddNews() throws Exception {
		News news = new News();
		news.setCreationDate(new Date());
		news.setModificationDate(calendar.getTime());
		news.setTitle("�����");
		news.setShortText("������� �������� ������ �����");
		news.setFullText("�������� ������ �������� ���� ������ ������ "
				+ "����� ��� ������ ����������. ��������-��������� �� "
				+ "������ ����� �������� ������ ����� �iasniar �� ������ "
				+ "��������� ����������� ������������ �����.� ������� ������� "
				+ "����������� �������� ����� � �����������, � ������� �����. � "
				+ "������ � ������ ����� ����, � ������ ������� ����������������, "
				+ "� �� ���� ������� �� ��� �������.");
		Long newsId = newsDao.create(news);
		news.setId(newsId);
		News result = newsDao.read(newsId);
		assertEquals(news, result);
	}
	
	@Test
	public void testUpdateNews() throws Exception {
		News news= newsDao.read(2L);
		news.setShortText("�����: ���� ���� � �������");
		newsDao.update(news);
		News result = newsDao.read(2L);
		assertEquals(news.getShortText(), result.getShortText());
	}
	
	@Test
	public void testReadNews() throws Exception {
		News news = new News();
		news.setId(2L);
		news.setTitle("�������������");
		news.setShortText("� ������ �������� ������������� �� ��������� � ����");
		news.setFullText("����� �� ������ ������������ ������� � �������� Onliner.by "
				+ "�������� �������. ����� �� ���������� � ��������� ��������� �� ������. "
				+ "����� ���� ��� ����� ����� ��� ����������� �� ��������� ������ �������� "
				+ "Mercedes, ������������� � ������� � ��� �� � ��� �� ������ ����, �������� "
				+ "������ �� ������������.���� 2-� ������������ ��������. ����� ���� ���, "
				+ "������ �� ������. � ��� ����� �������� ������ ���� ����� � ��� ��������� "
				+ "���� Mercedes. �� ��� ������ �������� ������ ���������� ����, ���, ��� �� ���? "
				+ "� ����� ������ �����, ��� � �� ��������� ��� ���� �� �� ����������, ����� ������� "
				+ "����������� ��� � �����, � ��������� �������.������� �������� ����������� � ������"
				+ " �������� �� ��� ������ �� ����. �� �� �� ������� ��������� ���� �������� ��� ��������. "
				+ "������� ����� � ��� ������, ���� ����� ����� ���������� ��� ����������� � ���������������, "
				+ "�� ������ ���������. ����� �� ����� �� ������. ���� �� ���������, ��� ��� ���������� �������"
				+ " �������� � ����, ���� ��� ���� �������� � ���� �������� ���������, � ������� ��� ��������." );
		news.setCreationDate(sdfTimeStamp.parse("2015-03-13 10:41:35 AM"));
		news.setModificationDate(sdfDate.parse("2015-03-14"));
		News result = newsDao.read(2L);
		assertEquals(news, result);
	}
	
}
