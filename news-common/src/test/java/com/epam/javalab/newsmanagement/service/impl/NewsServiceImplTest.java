package com.epam.javalab.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.javalab.newsmanagement.dao.INewsDao;
import com.epam.javalab.newsmanagement.entity.News;

public class NewsServiceImplTest {
	
	@Mock
	private INewsDao newsDao;
	@InjectMocks
	private NewsServiceImpl newsService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test 
	public void shouldCreateNews() throws Exception {
		News news = new News();
		news.setTitle("����������� �����������");
		news.setShortText("� ������ �� ������� ����������� ����� �����������");
		news.setFullText("����������� �����������, ������������������ � ������� "
				+ "�� �����, ��������� � ���������� ������ ������, �� ����� "
				+ "������� 2015 ���� ��������� 4 ���. �������, ��� � 2,1 ���� "
				+ "������, ��� �� ����� ������� 2014 ����, � �� 27,5% ������, ��� �� "
				+ "����� ������ 2015 ����, �������� � ������� �������������� ���������� �������.");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		when(newsDao.create(news)).thenReturn(1L);
		Long newsId = newsService.create(news);
		assertEquals(Long.valueOf(1L), newsId);
		verify(newsDao).create(news);
	}
	
	@Test
    public void shouldDeleteNews() throws Exception {
		Long newsId = 1L;
		doNothing().when(newsDao).delete(newsId);
		newsService.delete(newsId);
		verify(newsDao).delete(newsId);
	}
	
	@Test
	public void shouldUpdateNews() throws Exception {
		News news = new News();
		news.setId(5L);
		news.setTitle("����������� �����������");
		news.setShortText("� ������ �� ������� ����������� ����� �����������");
		news.setFullText("����������� �����������, ������������������ � ������� "
				+ "�� �����, ��������� � ���������� ������ ������, �� ����� "
				+ "������� 2015 ���� ��������� 4 ���. �������, ��� � 2,1 ���� "
				+ "������, ��� �� ����� ������� 2014 ����, � �� 27,5% ������, ��� �� "
				+ "����� ������ 2015 ����, �������� � ������� �������������� ���������� �������.");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		doNothing().when(newsDao).update(news);
		newsService.update(news);
		verify(newsDao).update(news);
	}
	
	@Test
	public void shouldReadNews() throws Exception {
		News news = new News();
		news.setId(5L);
		news.setTitle("����������� �����������");
		news.setShortText("� ������ �� ������� ����������� ����� �����������");
		news.setFullText("����������� �����������, ������������������ � ������� "
				+ "�� �����, ��������� � ���������� ������ ������, �� ����� "
				+ "������� 2015 ���� ��������� 4 ���. �������, ��� � 2,1 ���� "
				+ "������, ��� �� ����� ������� 2014 ����, � �� 27,5% ������, ��� �� "
				+ "����� ������ 2015 ����, �������� � ������� �������������� ���������� �������.");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		when(newsDao.read(1L)).thenReturn(news);
		News result = newsService.read(1L);
		assertEquals(news, result);
		verify(newsDao).read(1L);
	}
	
	@Test 
	public void shouldAttachNewsAuthor() throws Exception {
		Long newsId = 1L;
		Long authorId = 1L;
		doNothing().when(newsDao).attachNewsAuthor(newsId, authorId);
		newsService.attachNewsAuthor(newsId, authorId);
		verify(newsDao).attachNewsAuthor(newsId, authorId);
	}
	
	@Test
	public void shouldAttachNewsTags() throws Exception {
		Long newsId = 1L;
		Long[] tagIds = {1L, 2L, 3L};
		doNothing().when(newsDao).attachNewsTags(newsId, tagIds);
		newsService.attachNewsTags(newsId, tagIds);
		verify(newsDao).attachNewsTags(newsId, tagIds);
	}
	
	@Test 
	public void shouldDetachNewsAuthor() throws Exception {
		Long newsId = 1L;
		doNothing().when(newsDao).detachNewsAuthor(newsId);
		newsService.detachNewsAuthor(newsId);
		verify(newsDao).detachNewsAuthor(newsId);
	}
	
	@Test
	public void shouldDetachNewsTags() throws Exception {
		Long newsId = 1L;
		doNothing().when(newsDao).detachNewsTags(newsId);
		newsService.detachNewsTags(newsId);
		verify(newsDao).detachNewsTags(newsId);
	}
	
}
