package com.epam.javalab.newsmanagement.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.javalab.newsmanagement.dao.ITagDao;
import com.epam.javalab.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "/dbunit/TagTestDataSet.xml")
public class TagDaoJDBCImplTest {

	@Autowired
	private ITagDao tagDao;

	@Test
	public void testAddTag() throws Exception {
		Tag tag = new Tag();
		tag.setName("����");
		Long tagId = tagDao.create(tag);
		Tag result = tagDao.read(tagId);
		assertEquals(tag.getName(), result.getName());
		assertEquals(tagId, result.getId());
	}
	
	@Test
	public void testAllNewsTags() throws Exception {
		List<Tag> tags = tagDao.getAllNewsTags(1L);
		assertEquals(1L, tags.size());
		Tag tag = tags.get(0);
		assertEquals(new Long(1L), tag.getId());
		assertEquals("����", tag.getName());
	}
	
	@Test
	public void testDeleteTag() throws Exception {
		Long tagId = 3L;
		assertEquals(1, tagDao.getAllNewsTags(3L).size());
		tagDao.delete(tagId);
		assertTrue(tagDao.getAllNewsTags(3L).isEmpty());
	}
	
	@Test
	public void testUpdateTag() throws Exception {
		Tag tag = tagDao.read(1L);
		tag.setName("����");
		tagDao.update(tag);
		Tag result = tagDao.read(1L);
		assertEquals("����", result.getName());
	}
	
	@Test
	public void testReadTag() throws Exception {
		Tag result = tagDao.read(1L);
		assertEquals(new Long(1L), result.getId());
		assertEquals("����", result.getName());
	}
}
