package com.epam.javalab.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.javalab.newsmanagement.dao.ICommentDao;
import com.epam.javalab.newsmanagement.entity.Comment;
import com.epam.javalab.newsmanagement.service.impl.CommentServiceImpl;

public class CommentServiceImplTest {
	
	@Mock
	private ICommentDao commentDao;
	@InjectMocks
	private CommentServiceImpl commentService;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void shouldCreateComment() throws Exception {
		Comment comment = new Comment();
		comment.setNewsId(8L);
		comment.setCommentText("� ��� ��� ������ �� �����. �������.");
		String date = "28-03-2015 10:20:56";
		comment.setCreationDate(sdf.parse(date));
		when(commentDao.create(comment)).thenReturn(7L);
		Long commentId = commentService.create(comment);
		assertEquals(Long.valueOf(7L), commentId);
		verify(commentDao).create(comment);
	}
	
	@Test
	public void shouldUpdateComment() throws Exception {
		Comment comment = new Comment();
		comment.setId(10L);
		comment.setCommentText("��������� ����������, � ���� ���������");
		String date = "25-02-2015 15:30:56";
		comment.setCreationDate(sdf.parse(date));
		comment.setNewsId(20L);
		doNothing().when(commentDao).update(comment);
		commentService.update(comment);
		verify(commentDao).update(comment);
	}
	
	@Test
	public void shouldDeleteComment() throws Exception {
		Long commentId = 5L;
		doNothing().when(commentDao).delete(commentId);
		commentService.delete(commentId);
		verify(commentDao).delete(commentId);
	}
	
	@Test
	public void shouldReadComment() throws Exception {
		Comment comment = new Comment();
		comment.setId(15L);
		String date = "22-01-2015 12:03:35";
		comment.setCreationDate(sdf.parse(date));
		comment.setNewsId(8L);
		comment.setCommentText("� ��� �� ����� ����");
		when(commentDao.read(15L)).thenReturn(comment);
		Comment result = commentService.read(15L);
		assertEquals(comment, result);
		verify(commentDao).read(15L);
	}
	
	@Test
	public void shouldGetAllCommentsByNews() throws Exception {
		List<Comment> comments = new ArrayList<Comment>();
		String date = "23-03-2015 11:54:25";
		Comment comment = new Comment();
		comment.setId(12L);
		comment.setNewsId(10L);
		comment.setCommentText("�� �������. ����� �� �����, ����� �� ������.");
		comment.setCreationDate(sdf.parse(date));
		comments.add(comment);
		date = "25-03-2015 15:13:10";
		comment = new Comment();
		comment.setNewsId(10L);
		comment.setId(13L);
		comment.setCreationDate(sdf.parse(date));
		comment.setCommentText("��� �� �� �����!");
		comments.add(comment);
		when(commentDao.getAllNewsComments(10L)).thenReturn(comments);
		List<Comment> result = commentService.getAllNewsComments(10L);
		assertEquals(comments, result);
		verify(commentDao).getAllNewsComments(10L);
	}
}
