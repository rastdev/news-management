package com.epam.javalab.newsmanagement.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.javalab.newsmanagement.dao.IAuthorDao;
import com.epam.javalab.newsmanagement.entity.Author;
import com.epam.javalab.newsmanagement.exception.DAOException;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.impl.AuthorServiceImpl;

public class AuthorServiceImplTest {
	
	@Mock
	private IAuthorDao authorDao;
	@InjectMocks
	private AuthorServiceImpl authorService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void shouldCreateNewAuthor() throws Exception {
		Author author = new Author();
		author.setName("������ ����");
		when(authorDao.create(author)).thenReturn(1L);
		Long authorId = authorService.create(author);
		assertEquals(Long.valueOf(1), authorId);
		verify(authorDao).create(author);
	}
	
	@Test
	public void shouldReadAuthor() throws Exception {
		Author author = new Author();
		author.setId(5L);
		author.setName("�������	�����");
		when(authorDao.read(5L)).thenReturn(author);
		Author result = authorService.read(5L);
		assertEquals(author, result);
		verify(authorDao).read(5L);
	}
	
	@Test
	public void shouldUpdateAuthor() throws Exception {
		Author author = new Author();
		author.setId(5L);
		author.setName("������� ����");
		doNothing().when(authorDao).update(author);
		authorService.update(author);
		verify(authorDao).update(author);
	}
	
	@Test
	public void shouldDeleteAuthor() throws Exception {
		doNothing().when(authorDao).delete(7L);
		authorService.delete(7L);
		verify(authorDao).delete(7L);
	}
	
	@Test(expected=ServiceException.class)
    public void shouldNotUpdateIfAuthorNotFound() throws Exception {
		Author author = new Author();
		author.setId(1L);
		author.setName("������� �����");
        doThrow(DAOException.class).when(authorDao).update(author);
        authorService.update(author);
    }
	 
	@Test 
	public void shouldGetAllAuthors() throws Exception {
		List<Author> authors = new ArrayList<Author>();
		authors.add(new Author(5L, "�������� ��������"));
		authors.add(new Author(6L, "�������� ����"));
		authors.add(new Author(10L, "������� ������"));
		when(authorDao.getAllAuthors()).thenReturn(authors);
		List<Author> result = authorService.getAllAuthors();
		assertEquals(authors, result);
		verify(authorDao).getAllAuthors();
	}
	
	@Test
	public void shouldGetNewsAuthor() throws Exception {
		Author author= new Author();
		author.setId(10L);
		author.setName("������ ������");
		when(authorDao.getNewsAuthor(5L)).thenReturn(author);
		Author result = authorService.getNewsAuthor(5L);
		assertEquals(author, result);
		verify(authorDao).getNewsAuthor(5L);
	}
}
