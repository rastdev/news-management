package com.epam.javalab.newsmanagement.dao;

import java.util.List;

import com.epam.javalab.newsmanagement.entity.Comment;
import com.epam.javalab.newsmanagement.exception.DAOException;

/**
 * Provides additional methods to manage business logic which is particular for
 * the <tt>CommentTO</tt> object
 * 
 * @author Dzmitry Rastsiusheuski
 * 
 */
public interface ICommentDao extends IGenericDao<Comment, Long> {

	/**
	 * Gets all comments associated with the news by news id
	 * 
	 * @param newsId the news id
	 * @return the list of news
	 * @throws DAOException
	 */
	public List<Comment> getAllNewsComments(Long newsId) throws DAOException;
}
