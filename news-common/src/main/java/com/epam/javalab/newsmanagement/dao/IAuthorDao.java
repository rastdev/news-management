package com.epam.javalab.newsmanagement.dao;

import java.util.List;

import com.epam.javalab.newsmanagement.entity.Author;
import com.epam.javalab.newsmanagement.exception.DAOException;

/**
 * Provides additional methods to manage business logic which is associated with
 * Author entity
 * 
 * @author Dzmitry Rastsiusheuski
 * 
 */
public interface IAuthorDao extends IGenericDao<Author, Long> {

	/**
	 * Gets the news author by news id
	 * 
	 * @param newsId the news id
	 * @return the author entity
	 * @throws DAOException
	 */
	public Author getNewsAuthor(Long newsId) throws DAOException;
	
	/**
	 * Returns the list of available authors
	 * 
	 * @return list of authors
	 * @throws DAOException
	 */
	public List<Author> getAllAuthors() throws DAOException;
}
