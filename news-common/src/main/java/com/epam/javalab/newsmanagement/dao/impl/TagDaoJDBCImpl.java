package com.epam.javalab.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.javalab.newsmanagement.dao.ITagDao;
import com.epam.javalab.newsmanagement.dao.util.DAOUtil;
import com.epam.javalab.newsmanagement.entity.Tag;
import com.epam.javalab.newsmanagement.exception.DAOException;

/**
 * JDBC implementation of the <code>ITagDao</code> using Oracle 
 * <code>DataSource</code> connection pool and prepared queries
 * 
 * @author Dzmitry Rastsiusheuski
 *
 */
public class TagDaoJDBCImpl implements ITagDao {

	private final static String INSERT_TAG_SQL = "INSERT INTO TAG (TAG_NAME) VALUES(?)";
	private final static String DELETE_TAG_BY_ID_SQL = "DELETE FROM TAG WHERE TAG_ID = ?";
	private final static String UPDATE_TAG_BY_ID_SQL = "UPDATE TAG SET TAG_NAME = ? "
			+ "WHERE TAG_ID = ?";
	private final static String SELECT_ALL_TAGS_BY_NEWS_ID_SQL = "SELECT TAG.TAG_ID, TAG.TAG_NAME "
			+ "FROM TAG JOIN NEWS_TAG  ON TAG.TAG_ID = NEWS_TAG.TAG_ID WHERE NEWS_TAG.NEWS_ID = ?";
	private final static String SELECT_TAG_BY_ID_SQL = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";
	private final static String SELECT_ALL_TAGS_SQL = "SELECT TAG_ID, TAG_NAME FROM TAG ORDER BY TAG_NAME NULLS LAST";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Creates a new tag in database and returns generated primary key
	 * 
	 * @param newInstance the tag
	 * @return the primary key
	 * @throws DAOException if can't create a new tag
	 */
	@Override
	public Long create(Tag newInstance) throws DAOException {
		Long tagId = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet generatedKeys = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(INSERT_TAG_SQL, 
					new String[] { "TAG_ID" });
			ps.setString(1, newInstance.getName());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				generatedKeys = ps.getGeneratedKeys();
				if (null != generatedKeys && generatedKeys.next()) {
					tagId = generatedKeys.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DAOException("Can't create a tag", e);
		} finally {
			DAOUtil.close(generatedKeys, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tagId;
	}
	
	/**
	 * Extracts the tag previously added to database using specified id as
	 * primary key
	 * 
	 * @param tagId the primary key
	 * @return the tag
	 * @throws DAOException if there isn't such a tag in database 
	 */
	@Override
	public Tag read(Long tagId) throws DAOException {
		Tag tag = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps	= con.prepareStatement(SELECT_TAG_BY_ID_SQL);
			ps.setLong(1, tagId);
			rs = ps.executeQuery();
			while (rs.next()) {
				tag = parseResultSet(rs);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't read tag by id", e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tag;
	}
	
	/**
	 * Saves modified state of the tag in database
	 * 
	 * @param tag the tag to update
	 * @throws DAOException if there isn't such a tag or it has been updated 
	 * more than one tag
	 */
	@Override
	public void update(Tag tag) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try { 
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(UPDATE_TAG_BY_ID_SQL);
			ps.setString(1, tag.getName());
			ps.setLong(2, tag.getId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new DAOException("Updated more than one tag");
			}
		} catch (SQLException e) {
			throw new DAOException("Can't update tag by id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	/**
	 * Deletes the record about the tag from database
	 * 
	 * @param tag the tag id
	 * @throws DAOException if there isn't such a tag to delete or
	 *  the query deleted more or less than one tag
	 */
	@Override
	public void delete(Long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try { 
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DELETE_TAG_BY_ID_SQL);
			ps.setLong(1, tagId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new DAOException("Deleted more than one tag");
			}
		} catch (SQLException e) {
			throw new DAOException("Can't delete tag by id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	/**
	 * Gets all tags associated with the news by news id
	 * 
	 * @param newsId the news id
	 * @return the list of tags
	 * @throws DAOException if can't get all news tags
	 */
	@Override
	public List<Tag> getAllNewsTags(Long newsId) throws DAOException {
		List<Tag> tags = new ArrayList<Tag>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
		    ps = con.prepareStatement(SELECT_ALL_TAGS_BY_NEWS_ID_SQL);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Tag tag = parseResultSet(rs);
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't get all news tags", e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tags;
	}
	
	/**
	 * Returns list of news tags 
	 * 
	 * @return list of tags
	 * @throws DAOException
	 */
	@Override
	public List<Tag> getAllTags() throws DAOException {
		List<Tag> tags = new ArrayList<Tag>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
		    st = con.createStatement();
			rs = st.executeQuery(SELECT_ALL_TAGS_SQL);
			while (rs.next()) {
				Tag tag = parseResultSet(rs);
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't get all news tags", e);
		} finally {
			DAOUtil.close(rs, st);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tags;
	}

	
	/**
	 * Extracts the data returned by the query from the result set and pack it
	 * into the <code>Tag</code>
	 * 
	 * @param rs the result set of the executed query
	 * @return the tag entity
	 * @throws SQLException
	 */
	public Tag parseResultSet(ResultSet rs) throws SQLException {
		Tag tag = new Tag();
		tag.setId(rs.getLong("TAG_ID"));
		tag.setName(rs.getString("TAG_NAME"));
		return tag;
	}
}
