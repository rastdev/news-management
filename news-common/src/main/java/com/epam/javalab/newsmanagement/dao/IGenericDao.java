package com.epam.javalab.newsmanagement.dao;

import java.io.Serializable;

import com.epam.javalab.newsmanagement.exception.DAOException;
/**
 * Unified interface which is responsible for providing basic C.R.U.D operations
 * 
 * @author Dzmitry Rastsiusheuski
 *
 * @param <T> the entity type
 * @param <PK> the primary key type
 */
public interface IGenericDao<T, PK extends Serializable> {

	/**
	 * Creates new record in database using appropriate object
	 * 
	 * @param newInstance the entity 
	 * @return the primary key
	 * @throws DAOException 
	 */
	public PK create(T newInstance) throws DAOException;

	/**
	 * Extracts the object previously added to database using specified id as
	 * primary key
	 * 
	 * @param key the primary key
	 * @return the entity
	 * @throws DAOException 
	 */
	public T read(PK id) throws DAOException;

	/**
	 * Saves modified state of object in database
	 * 
	 * @param transientObject the entity to update
	 * @throws DAOException 
	 */
	public void update(T updatedInstance) throws DAOException;

	/**
	 * Deletes record about the object from database
	 * 
	 * @param persistentObject the entity to delete
	 * @throws DAOException 
	 */
	public void delete(PK id) throws DAOException;
}
