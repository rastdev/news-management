package com.epam.javalab.newsmanagement.service;

import java.util.List;

import com.epam.javalab.newsmanagement.entity.Comment;
import com.epam.javalab.newsmanagement.exception.ServiceException;

public interface ICommentService extends IGenericService<Comment, Long> {
	
	/**
	 * Gets all comments associated with the news by news id
	 * 
	 * @param newsId the news id
	 * @return the list of news
	 * @throws ServiceException if can't get all comments by news id
	 */
	public List<Comment> getAllNewsComments(Long newsId)
			throws ServiceException;
}
