package com.epam.javalab.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.javalab.newsmanagement.entity.Author;
import com.epam.javalab.newsmanagement.entity.Comment;
import com.epam.javalab.newsmanagement.entity.News;
import com.epam.javalab.newsmanagement.entity.NewsTO;
import com.epam.javalab.newsmanagement.entity.SearchCriteria;
import com.epam.javalab.newsmanagement.entity.Tag;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.IAuthorService;
import com.epam.javalab.newsmanagement.service.ICommentService;
import com.epam.javalab.newsmanagement.service.INewsManagementService;
import com.epam.javalab.newsmanagement.service.INewsService;
import com.epam.javalab.newsmanagement.service.ITagService;

/**
 * Main service which includes all the particular services
 * 
 * @author Dzmitry Rastsiusheuski
 *
 */
public class NewsManagementServiceImpl implements INewsManagementService {

	private INewsService newsService;
	private IAuthorService authorService;
	private ICommentService commentService;
	private ITagService tagService;

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsManagementService#createNews(com.epam.javalab.newsmanagement.entity.News, java.lang.Long, java.lang.Long[])
	 */
	@Transactional
	public Long createNews(News news, Long authorId, Long... tagIds)
			throws ServiceException {
		Long newsId = newsService.create(news);
		newsService.attachNewsAuthor(newsId, authorId);
		if (tagIds != null) {
			newsService.attachNewsTags(newsId, tagIds);
		}
		return newsId;
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsManagementService#updateNews(com.epam.javalab.newsmanagement.entity.News, java.lang.Long, java.lang.Long[])
	 */
	@Transactional
	public void updateNews(News news, Long authorId, Long... tagIds)
			throws ServiceException {
		Long newsId = news.getId();
		newsService.update(news);
		newsService.detachNewsAuthor(newsId);
		newsService.attachNewsAuthor(newsId, authorId);
		newsService.detachNewsTags(newsId);
		if (tagIds != null) {
			newsService.attachNewsTags(newsId, tagIds);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsManagementService#readNews(java.lang.Long)
	 */
	public NewsTO readNews(Long key) throws ServiceException {
		News news = newsService.read(key);
		List<Tag> tags = tagService.getAllNewsTags(key);
		Author author = authorService.getNewsAuthor(key);
		List<Comment> comments = commentService.getAllNewsComments(key);
		NewsTO newsTO = new NewsTO();
		newsTO.setAuthor(author);
		newsTO.setComments(comments);
		newsTO.setTags(tags);
		newsTO.setNews(news);
		return newsTO;
	}


	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsManagementService#getLimitNews(int, int, com.epam.javalab.newsmanagement.entity.SearchCriteria)
	 */
	@Override
	public List<NewsTO> getLimitNews(int from, int offset, SearchCriteria sc)
			throws ServiceException {
		List<News> newsList = newsService.getLimitNews(from, offset, sc);
		List<NewsTO> newsTOList = new ArrayList<NewsTO>();
		for (News news : newsList) {
			newsTOList.add(readNews(news.getId()));
		}
		return newsTOList;
	}

}
