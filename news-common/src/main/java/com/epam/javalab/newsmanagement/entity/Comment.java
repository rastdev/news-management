package com.epam.javalab.newsmanagement.entity;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

import com.epam.javalab.newsmanagement.util.JsonCommetSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
/**
 * Represents the comment entity
 * 
 * @author Dzmitry Rastsiusheuski
 *
 */
@JsonSerialize(using = JsonCommetSerializer.class)
public class Comment implements Serializable {

	private static final long serialVersionUID = -483026142646296220L;
	
	/**
	 * The primary key
	 */
	private Long id;
	/**
	 * Date of creation comment
	 */
	private Date creationDate;
	/**
	 * Text commentary
	 */
	private String commentText;
	/**
	 * Id of commented news  
	 */
	private Long newsId;

	private DateFormat dateFormat;
	
	public Comment() {
		super();
	}

	public Comment(Long id, Date creationDate, String commentText, Long newsId) {
		super();
		this.id = id;
		this.creationDate = creationDate;
		this.commentText = commentText;
		this.newsId = newsId;
	}

	public Long getId() {
		return id;
	}

	public DateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(DateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}
	
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CommentTO [id=" + id + ", creationDate=" + creationDate
				+ ", commentText=" + commentText + ", newsId=" + newsId + "]";
	}

}
