package com.epam.javalab.newsmanagement.service;

import java.util.List;

import com.epam.javalab.newsmanagement.entity.Tag;
import com.epam.javalab.newsmanagement.exception.ServiceException;

public interface ITagService extends IGenericService<Tag, Long> {
	
	/**
	 * Gets the list of news tags attached to the news
	 * 
	 * @param newsId the news id
	 * @return list of tags
	 * @throws ServiceException
	 */
	public List<Tag> getAllNewsTags(Long newsId) throws ServiceException;

	/**
	 * Returns the list of available news tags
	 * 
	 * @return list of tags
	 * @throws ServiceException
	 */
	public List<Tag> getAllTags() throws ServiceException;
}
