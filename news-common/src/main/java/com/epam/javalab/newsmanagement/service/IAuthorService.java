package com.epam.javalab.newsmanagement.service;

import java.util.List;

import com.epam.javalab.newsmanagement.entity.Author;
import com.epam.javalab.newsmanagement.exception.ServiceException;

public interface IAuthorService extends IGenericService<Author, Long> {
	
	/**
	 * Returns the list of available authors
	 * 
	 * @return list of authors
	 * @throws ServiceException
	 */
	public List<Author> getAllAuthors() throws ServiceException;
	
	/**
	 * Gets the news author by news id
	 * 
	 * @param newsId the news id
	 * @return the author entity
	 * @throws ServiceException
	 */
	public Author getNewsAuthor(Long newsId) throws ServiceException;
}
