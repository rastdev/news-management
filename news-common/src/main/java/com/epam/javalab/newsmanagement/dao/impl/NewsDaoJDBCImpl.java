package com.epam.javalab.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.javalab.newsmanagement.dao.INewsDao;
import com.epam.javalab.newsmanagement.dao.util.DAOUtil;
import com.epam.javalab.newsmanagement.entity.News;
import com.epam.javalab.newsmanagement.entity.SearchCriteria;
import com.epam.javalab.newsmanagement.exception.DAOException;

/**
 * JDBC implementation of the <code>INewsDao</code> using Oracle <code>DataSource</code>
 * connection pool and prepared queries
 * 
 * @author Dzmitry Rastsiusheuski
 *
 */
public class NewsDaoJDBCImpl implements INewsDao {

	private final static String INSERT_NEWS_SQL = "INSERT INTO NEWS (SHORT_TEXT, "
			+ "FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) VALUES(?, ?, ?, ?, ?)";
	private final static String SELECT_NEWS_BY_ID_SQL = "SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE,"
			+ " MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
	private final static String UPDATE_NEWS_BY_ID_SQL = "UPDATE NEWS SET SHORT_TEXT = ?, FULL_TEXT = ?, "
			+ "TITLE = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
	private final static String DELETE_NEWS_BY_ID_SQL = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private final static String ATTACH_NEWS_AUTHOR_SQL = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
	private final static String DETACH_NEWS_AUTHOR_SQL = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
	private final static String ATTACH_NEWS_TAG_SQL = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES(?, ?)";
	private final static String DETACH_NEWS_TAG_SQL = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
	private final static String SELECT_COUNT_NEWS_PATTERN_SQL = "SELECT COUNT(*) FROM (SELECT DISTINCT N.NEWS_ID , N.SHORT_TEXT, N.FULL_TEXT, "
			+ "N.TITLE, N.CREATION_DATE, N.MODIFICATION_DATE FROM NEWS N %s)";
	private final static String SELECT_LIMIT_NEWS_PATTERN_SQL = "SELECT * FROM (SELECT  x.*, rownum as r FROM (SELECT DISTINCT N.NEWS_ID , N.SHORT_TEXT, N.FULL_TEXT, "
			+ "N.TITLE, N.CREATION_DATE, N.MODIFICATION_DATE, C.CNT FROM NEWS N LEFT JOIN (SELECT NEWS_ID, COUNT(*) CNT FROM "
			+ "COMMENTS GROUP BY NEWS_ID) C ON N.NEWS_ID = C.NEWS_ID %s ORDER BY C.CNT DESC NULLS LAST, N.MODIFICATION_DATE DESC) x) where r between ? AND ?";
	private final static String SELECT_NEXT_NEWS_ID_PATTERN_SQL = "WITH numbered_query AS (SELECT q.*, rownum as idx FROM (SELECT DISTINCT N.NEWS_ID , N.SHORT_TEXT, N.FULL_TEXT, "
			+ "N.TITLE, N.CREATION_DATE, N.MODIFICATION_DATE, C.CNT FROM NEWS N LEFT JOIN (SELECT NEWS_ID, COUNT(*) CNT FROM COMMENTS GROUP BY NEWS_ID) C ON N.NEWS_ID = C.NEWS_ID %s "
			+ "ORDER BY C.CNT DESC NULLS LAST, N.MODIFICATION_DATE DESC ) q) select q.* FROM numbered_query q, (SELECT MIN(idx) AS idx FROM numbered_query WHERE news_id = ?) ni where q.idx = ni.idx + 1";
	private final static String SELECT_PREVIOUS_NEWS_ID_PATTERN_SQL = "WITH numbered_query AS (SELECT q.*, rownum as idx FROM (SELECT DISTINCT N.NEWS_ID , N.SHORT_TEXT, N.FULL_TEXT, "
			+ "N.TITLE, N.CREATION_DATE, N.MODIFICATION_DATE, C.CNT FROM NEWS N LEFT JOIN (SELECT NEWS_ID, COUNT(*) CNT FROM COMMENTS GROUP BY NEWS_ID) C ON N.NEWS_ID = C.NEWS_ID %s "
			+ "ORDER BY C.CNT DESC NULLS LAST, N.MODIFICATION_DATE DESC) q) select q.NEWS_ID FROM numbered_query q, (SELECT MIN(idx) AS idx FROM numbered_query WHERE news_id = ?) ni where q.idx = ni.idx - 1";
	
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Creates news in database and returns generated primary key
	 * 
	 * @param newInstance the news
	 * @return the primary key
	 * @throws DAOException if can't create a news
	 */
	@Override
	public Long create(News newInstance) throws DAOException {
		Long newsId = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet generatedKeys = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(INSERT_NEWS_SQL,
					new String[] { "NEWS_ID" });
			ps.setString(1, newInstance.getShortText());
			ps.setString(2, newInstance.getFullText());
			ps.setString(3, newInstance.getTitle());
			ps.setTimestamp(4, new Timestamp(newInstance.getCreationDate()
					.getTime()));
			ps.setDate(5, new java.sql.Date(newInstance.getModificationDate()
					.getTime()));
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				generatedKeys = ps.getGeneratedKeys();
				if (null != generatedKeys && generatedKeys.next()) {
					newsId = generatedKeys.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DAOException("Can't create news", e);
		} finally {
			DAOUtil.close(generatedKeys, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsId;
	}

	/**
	 * Extracts the news previously added to database using specified id as
	 * primary key
	 * 
	 * @param newsId the primary key
	 * @return the news
	 * @throws DAOException if there isn't such news in database
	 */
	@Override
	public News read(Long newsId) throws DAOException {
		News news = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SELECT_NEWS_BY_ID_SQL);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				news = parseResultSet(rs);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't read news by id", e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return news;
	}

	/**
	 * Saves modified state of the news in database
	 * 
	 * @param news the news to update
	 * @throws DAOException if there isn't such a news or 
	 * it has been updated more than one news
	 */
	@Override
	public void update(News news) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(UPDATE_NEWS_BY_ID_SQL);
			ps.setString(1, news.getShortText());
			ps.setString(2, news.getFullText());
			ps.setString(3, news.getTitle());
			ps.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			ps.setDate(5, new java.sql.Date(news.getModificationDate()
					.getTime()));
			ps.setLong(6, news.getId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new DAOException("Updated more than one news");
			}
		} catch (SQLException e) {
			throw new DAOException("Can't update news by id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/**
	 * Deletes the record about the news from database
	 * 
	 * @param newsId the news id
	 * @throws DAOException if there isn't such a news to 
	 * delete or the query deleted more or less than one news
	 */
	@Override
	public void delete(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DELETE_NEWS_BY_ID_SQL);
			ps.setLong(1, newsId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new DAOException("Deleted more than one news");
			}
		} catch (SQLException e) {
			throw new DAOException("Can't delete news by id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.INewsDao#deleteNewsList(java.lang.Long[])
	 */
	@Override
	public void deleteNewsList(Long[] newsIdList) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DELETE_NEWS_BY_ID_SQL);
			for (Long newsId : newsIdList) {
				ps.setLong(1, newsId);
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Can't delete batch of news by id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.INewsDao#attachNewsAuthor(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void attachNewsAuthor(Long newsId, Long authorId)
			throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(ATTACH_NEWS_AUTHOR_SQL);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new DAOException(
						"Attached more than one author to the news");
			}
		} catch (SQLException e) {
			throw new DAOException("Can't attach the author to the news", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.INewsDao#attachNewsTags(java.lang.Long, java.lang.Long[])
	 */
	@Override
	public void attachNewsTags(Long newsId, Long[] tagIds) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(ATTACH_NEWS_TAG_SQL);
			ps.setLong(1, newsId);
			for (Long tagId : tagIds) {
				ps.setLong(2, tagId);
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Can't attach tags to the news", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.INewsDao#detachNewsTags(java.lang.Long)
	 */
	@Override
	public void detachNewsTags(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DETACH_NEWS_TAG_SQL);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't detach tags from the news by news id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.INewsDao#detachNewsAuthor(java.lang.Long)
	 */
	@Override
	public void detachNewsAuthor(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DETACH_NEWS_AUTHOR_SQL);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Can't detach author from the news by news id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.INewsDao#count(com.epam.javalab.newsmanagement.entity.SearchCriteria)
	 */
	public int count(SearchCriteria searchCriteria) throws DAOException {
		int count = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DAOUtil.buildQuery(searchCriteria,
					SELECT_COUNT_NEWS_PATTERN_SQL));
			rs = ps.executeQuery();

			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return count;
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.INewsDao#findNextNewsId(com.epam.javalab.newsmanagement.entity.SearchCriteria, java.lang.Long)
	 */
	public Long findNextNewsId(SearchCriteria searchCriteria, Long id)
			throws DAOException {
		Long result = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DAOUtil.buildQuery(searchCriteria,
					SELECT_NEXT_NEWS_ID_PATTERN_SQL));
			ps.setLong(1, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.INewsDao#findPreviousNewsId(com.epam.javalab.newsmanagement.entity.SearchCriteria, java.lang.Long)
	 */
	public Long findPreviousNewsId(SearchCriteria searchCriteria, Long id)
			throws DAOException {
		Long result = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DAOUtil.buildQuery(searchCriteria,
					SELECT_PREVIOUS_NEWS_ID_PATTERN_SQL));
			ps.setLong(1, id);
			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.INewsDao#getLimitNews(int, int, com.epam.javalab.newsmanagement.entity.SearchCriteria)
	 */
	@Override
	public List<News> getLimitNews(int from, int offset, SearchCriteria sc)
			throws DAOException {
		List<News> newsList = new ArrayList<News>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DAOUtil.buildQuery(sc,
					SELECT_LIMIT_NEWS_PATTERN_SQL));
			ps.setInt(1, from);
			ps.setInt(2, offset);
			rs = ps.executeQuery();
			while (rs.next()) {
				News news = parseResultSet(rs);
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't get list of all news", e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsList;
	}
	
	/**
	 * Extracts the data returned by the query from the result set and pack it
	 * into the <code>News</code>
	 * 
	 * @param rs the result set of the executed query
	 * @return the news entity
	 * @throws SQLException
	 */
	public News parseResultSet(ResultSet rs) throws SQLException {
		News news = new News();
		news.setId(rs.getLong("NEWS_ID"));
		news.setShortText(rs.getString("SHORT_TEXT"));
		news.setFullText(rs.getString("FULL_TEXT"));
		news.setTitle(rs.getString("TITLE"));
		news.setCreationDate(new Date(rs.getTimestamp("CREATION_DATE")
				.getTime()));
		news.setModificationDate(new Date(rs.getDate("MODIFICATION_DATE")
				.getTime()));
		return news;
	}

}
