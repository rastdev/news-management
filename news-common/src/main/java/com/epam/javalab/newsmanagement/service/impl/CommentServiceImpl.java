package com.epam.javalab.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.javalab.newsmanagement.dao.ICommentDao;
import com.epam.javalab.newsmanagement.entity.Comment;
import com.epam.javalab.newsmanagement.exception.DAOException;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.ICommentService;

public class CommentServiceImpl implements ICommentService {
	
	private final static Logger logger = Logger.getLogger(CommentServiceImpl.class);
	private ICommentDao commentDao;

	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}
	
	/**
	 * Creates a new comment and returns generated primary key
	 * 
	 * @param newInstance the comment
	 * @return the primary key
	 * @throws ServiceException if can't create a new comment
	 */
	@Override
	public Long create(Comment newInstance) throws ServiceException {
		try {
			return commentDao.create(newInstance);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Reads the comment previously added using specified id as
	 * primary key
	 * 
	 * @param key the primary key
	 * @return the author
	 * @throws ServiceException if there isn't such a comment
	 */
	@Override
	public Comment read(Long commentId) throws ServiceException {
		try {
			return commentDao.read(commentId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	/**
	 * Saves modified state of the comment
	 * 
	 * @param comment the comment to update
	 * @throws ServiceException if there isn't such a comment or it has been updated more
	 * than one comment
	 */
	@Override
	public void update(Comment comment) throws ServiceException {
		try {
			commentDao.update(comment);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Deletes the record about the comment
	 * 
	 * @param commentId the comment id
	 * @throws ServiceException if there isn't such a comment to delete or the query deleted
	 * more or less than one comment
	 */
	@Override
	public void delete(Long commentId) throws ServiceException {
		try {
			commentDao.delete(commentId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.ICommentService#getAllNewsComments(java.lang.Long)
	 */
	@Override
	public List<Comment> getAllNewsComments(Long newsId)
			throws ServiceException {
		try {
			return commentDao.getAllNewsComments(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

}
