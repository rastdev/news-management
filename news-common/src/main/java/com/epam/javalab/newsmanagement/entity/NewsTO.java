package com.epam.javalab.newsmanagement.entity;

import java.util.ArrayList;
import java.util.List;

public final class NewsTO {

	private Author author;
	private News news;
	private List<Tag> tags;
	private List<Comment> comments;

	public NewsTO() {
		tags = new ArrayList<Tag>();
		comments = new ArrayList<Comment>();
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

}
