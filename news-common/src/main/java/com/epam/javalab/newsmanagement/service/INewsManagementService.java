package com.epam.javalab.newsmanagement.service;

import java.util.List;

import com.epam.javalab.newsmanagement.entity.News;
import com.epam.javalab.newsmanagement.entity.NewsTO;
import com.epam.javalab.newsmanagement.entity.SearchCriteria;
import com.epam.javalab.newsmanagement.exception.ServiceException;

public interface INewsManagementService {

	/**
	 * Creates news and returns generated primary key.
	 * Attachs specifiend tags and author to the news.
	 * 
	 * @param news the news to create
	 * @param authorId the author id
	 * @param tagIds list of tag identifiers
	 * @return the primary key
	 * @throws ServiceException
	 */
	public Long createNews(News news, Long authorId, Long... tagIds)
			throws ServiceException;
	
	/**
	 * Reads the news previously added using specified id as primary key.
	 * Receives all the information related to the news and sets it into <code>NewsTO<code>
	 * 
	 * @param key the primary key
	 * @return the news
	 * @throws ServiceException if there isn't such news
	 */
	public NewsTO readNews(Long key) throws ServiceException;
	
	public void updateNews(News news, Long authorId, Long... tagIds) throws ServiceException;
	
	/**
	 * Returns list of news according to the search criteria in specified range 
	 * Finds and sets all the information about the news into <code>NewsTO<code>.
	 * 
	 * @param from the number of the record
	 * @param offset count of news to get
	 * @param sc the search criteria to look for the news
	 * @return list of news
	 * @throws ServiceException
	 */
	public List<NewsTO> getLimitNews(int from, int offset, SearchCriteria sc)
			throws ServiceException;

}
