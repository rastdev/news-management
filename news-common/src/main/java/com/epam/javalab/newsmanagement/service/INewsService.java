package com.epam.javalab.newsmanagement.service;

import java.util.List;

import com.epam.javalab.newsmanagement.entity.News;
import com.epam.javalab.newsmanagement.entity.SearchCriteria;
import com.epam.javalab.newsmanagement.exception.DAOException;
import com.epam.javalab.newsmanagement.exception.ServiceException;

public interface INewsService extends IGenericService<News, Long> {

	public void deleteNewsList(Long[] newsIdList) throws ServiceException;
	
	/**
	 * Attaches the author to the news using <code>newsId</code> and
	 * <code>authorId</code>
	 * 
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws ServiceException if can't attach the author 
	 * to the news or it has been attached more than one author to the news
	 */
	public void attachNewsAuthor(Long newsId, Long authorId)
			throws ServiceException;
	
	/**
	 * Adds specified tags to the news by the news id
	 * 
	 * @param newsId the news id
	 * @param tagIdList list of tag identifiers
	 * @throws ServiceException if can't attach tags to the news
	 */
	public void attachNewsTags(Long newsId, Long[] tagIdList)
			throws ServiceException;
	
	/**
	 * Returns total amount of rows according to the search criteria
	 * 
	 * @param sc the search criteria to look for
	 * @return Total amount of rows
	 * @throws ServiceException If something fails at DAO level
	 */
	public int count(SearchCriteria sc) throws ServiceException;
	
	/**
	 * Returns the next news id that follow the passed news id.
	 * If SearchCriteria is not empty the query will be filtered in advance. 
	 * 
	 * @param id the news id
 	 * @param searchCriteria the search criteria to look for the news
	 * @return the next news id
	 * @throws DAOException 
	 */
	public Long findNextNewsId(SearchCriteria searchCriteria, Long id)
			throws ServiceException;

	/**
	 * Returns the previous news id that is located before the passed news id.
	 * If SearchCriteria is not empty the query will be filtered in advance. 
	 * 
	 * @param id the news id
 	 * @param searchCriteria the search criteria to look for the news
	 * @return the previous news id
	 * @throws DAOException
	 */
	public Long findPreviousNewsId(SearchCriteria searchCriteria, Long id)
			throws ServiceException;
	
	/**
	 * Deletes all the tags from the news by news id
	 * 
	 * @param newsId the news id
	 * @throws DAOException if can't detach tags from the news
	 */
	public void detachNewsTags(Long newsId) throws ServiceException;
	
	/**
	 * Detaches the news author from the news by news id
	 * 
	 * @param newsId the news id
	 * @throws DAOException if can't detach author from the news
	 */
	public void detachNewsAuthor(Long newsId) throws ServiceException;
	
	/**
	 * Returns list of news according to the search criteria in specified range 
	 * 
	 * @param from the number of the record
	 * @param offset count of news to get
	 * @param sc the search criteria to look for the news
	 * @return list of news
	 * @throws ServiceException
	 */
	public List<News> getLimitNews(int from, int offset, SearchCriteria sc)
			throws ServiceException;

}
