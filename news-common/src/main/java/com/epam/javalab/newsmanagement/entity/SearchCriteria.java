package com.epam.javalab.newsmanagement.entity;

import java.util.List;

public class SearchCriteria {
	
	private Long authorId;
	private List<Long> tagIds;
	
	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIds() {
		return tagIds;
	}
	
	public void setTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}
	
}
