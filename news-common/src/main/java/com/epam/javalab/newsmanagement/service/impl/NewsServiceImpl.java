package com.epam.javalab.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.javalab.newsmanagement.dao.INewsDao;
import com.epam.javalab.newsmanagement.entity.News;
import com.epam.javalab.newsmanagement.entity.SearchCriteria;
import com.epam.javalab.newsmanagement.exception.DAOException;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.INewsService;

public class NewsServiceImpl implements INewsService {

	private final static Logger logger = Logger
			.getLogger(NewsServiceImpl.class);
	private INewsDao newsDao;

	public void setNewsDao(INewsDao newsDao) {
		this.newsDao = newsDao;
	}

	/**
	 * Creates news and returns generated primary key
	 * 
	 * @param newInstance the news
	 * @return the primary key
	 * @throws ServiceException if can't create a news
	 */
	@Override
	public Long create(News newInstance) throws ServiceException {
		try {
			return newsDao.create(newInstance);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Reads the news previously added using specified id as primary key
	 * 
	 * @param newsId the primary key
	 * @return the news
	 * @throws ServiceException if there isn't such news
	 */
	@Override
	public News read(Long newsId) throws ServiceException {
		try {
			return newsDao.read(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Saves modified state of the news
	 * 
	 * @param news the news to update
	 * @throws ServiceException if there isn't such a news 
	 * or it has been updated more than one news
	 */
	@Override
	public void update(News news) throws ServiceException {
		try {
			newsDao.update(news);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/**
	 * Deletes the record about the news
	 * 
	 * @param newsId the news id
	 * @throws ServiceException if there isn't such a news 
	 * to delete or the query deleted more or less than one news
	 */
	@Override
	public void delete(Long newsId) throws ServiceException {
		try {
			newsDao.delete(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	public void deleteNewsList(Long[] newsIdList) throws ServiceException {
		try {
			newsDao.deleteNewsList(newsIdList);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsService#attachNewsAuthor(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void attachNewsAuthor(Long newsId, Long authorId)
			throws ServiceException {
		try {
			newsDao.attachNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsService#attachNewsTags(java.lang.Long, java.lang.Long[])
	 */
	@Override
	public void attachNewsTags(Long newsId, Long[] tagIdList)
			throws ServiceException {
		try {
			newsDao.attachNewsTags(newsId, tagIdList);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsService#count(com.epam.javalab.newsmanagement.entity.SearchCriteria)
	 */
	public int count(SearchCriteria sc) throws ServiceException {
		try {
			return newsDao.count(sc);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsService#findNextNewsId(com.epam.javalab.newsmanagement.entity.SearchCriteria, java.lang.Long)
	 */
	@Override
	public Long findNextNewsId(SearchCriteria searchCriteria, Long id)
			throws ServiceException {
		try {
			return newsDao.findNextNewsId(searchCriteria, id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsService#findPreviousNewsId(com.epam.javalab.newsmanagement.entity.SearchCriteria, java.lang.Long)
	 */
	@Override
	public Long findPreviousNewsId(SearchCriteria searchCriteria, Long id)
			throws ServiceException {
		try {
			return newsDao.findPreviousNewsId(searchCriteria, id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}


	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsService#getLimitNews(int, int, com.epam.javalab.newsmanagement.entity.SearchCriteria)
	 */
	@Override
	public List<News> getLimitNews(int from, int offset, SearchCriteria sc)
			throws ServiceException {
		try {
			return newsDao.getLimitNews(from, offset, sc);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsService#detachNewsTags(java.lang.Long)
	 */
	@Override
	public void detachNewsTags(Long newsId) throws ServiceException {
		try {
			newsDao.detachNewsTags(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.INewsService#detachNewsAuthor(java.lang.Long)
	 */
	@Override
	public void detachNewsAuthor(Long newsId) throws ServiceException {
		try {
			newsDao.detachNewsAuthor(newsId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}
}
