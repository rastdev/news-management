package com.epam.javalab.newsmanagement.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Formatter;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.javalab.newsmanagement.entity.SearchCriteria;

/**
 * Utilites class which perfomrs routine database operations, for example 
 * close database connection.
 * 
 * @author Dzmitry Rastsiusheuski
 *
 */
public class DAOUtil {

	private final static Logger logger = Logger.getLogger(DAOUtil.class);
	
	/**
	 * Builds a query by pattern which is previously prepared with 
	 * SearchCriteria 
	 * 
	 * @param sc the serach criteria
	 * @param pattern the pattern to insert in
	 * @return prepared query
	 */
	public static String buildQuery(SearchCriteria sc, String pattern) {
		Formatter f = new Formatter();
		f.format(pattern, concatSerachCriteria(sc));
		String resultQuery = f.toString();
		f.close();

		return resultQuery;
	}
	
	/**
	 * Creates IN section from the list of identifiers 
	 * for the query
	 * 
	 * @param idList the list of identifiers
	 * @return IN section
	 */
	private static String buildInSection(List<Long> idList) {
		StringBuffer builder = new StringBuffer();
		builder.setLength(0);
		builder.append("(");
		for (Long id : idList) {
			builder.append(id + ",");
		}
		// Deletes the last comma
		builder.deleteCharAt(builder.length() - 1);
		builder.append(")");
		return builder.toString();
	}
	
	/**
	 * 	Concats the part of the query to search for the news 
	 *  by Search Criteria
	 *  
	 * @param sc the serach criteria
	 * @return the part of the query for searching by the criterion
	 */
	private static String concatSerachCriteria(SearchCriteria sc) {
		String joinTags = "JOIN  NEWS_TAG NT ON NT.NEWS_ID = N.NEWS_ID AND NT.TAG_ID IN %s ";
		String joinAuthors = "JOIN NEWS_AUTHOR NA ON NA.NEWS_ID = N.NEWS_ID AND NA.AUTHOR_ID = %s ";
		Formatter f = new Formatter();

		List<Long> tagIds = sc.getTagIds();
		Long authorId = sc.getAuthorId();

		if (tagIds != null) {
			f.format(joinTags, buildInSection(tagIds));
		}
		if (authorId != null) {
			f.format(joinAuthors, authorId);
		}

		String resultQuery = f.toString();
		f.close();

		return resultQuery;
	}
	
	/**
     * Close a Connection, Statement and 
     * ResultSet.  Avoid closing if null and hide any 
     * SQLExceptions that occur.
     */
	public static void close(ResultSet rs, Statement st, Connection con) {

		if (null != rs) {
			try {
				rs.close();
			} catch (SQLException e) {
				logger.warn("Can't close result set");
			}
		}


		if (null != st) {
			try {
				st.close();
			} catch (SQLException e) {
				logger.warn("Can't close properly statement");
			}
		}
		
		if (null != con) {
			try {
				con.close();
			} catch (SQLException e) {
				logger.warn("Can't close connection");
			}
		}
	}
	
	/**
     * Close a Statement and 
     * ResultSet.  Avoid closing if null and hide any 
     * SQLExceptions that occur.
     */
	public static void close(ResultSet rs, Statement st) {
		close(rs, st, null);
	}
	
	/**
     * Close a Statement and 
     * Connection.  Avoid closing if null and hide any 
     * SQLExceptions that occur.
     */
	public static void close(Statement st, Connection con) {
		close(null, st, con);
	}
	
	/**
     * Close a Statement. Avoid closing if null and hide any 
     * SQLExceptions that occur.
     */
	public static void close(Statement st) {
		close(null, st, null);
	}

}
