package com.epam.javalab.newsmanagement.service;

import java.io.Serializable;

import com.epam.javalab.newsmanagement.exception.ServiceException;
/**
 * Unified service which is responsible for providing basic C.R.U.D operations
 * 
 * @author Dzmitry Rastsiusheuski
 *
 * @param <T> the entity type
 * @param <PK> the primary key type
 */
public interface IGenericService<T, PK extends Serializable> {
	
	/**
	 * Creates new entity using appropriate object
	 * 
	 * @param newInstance the entity 
	 * @return the primary key
	 * @throws ServiceException 
	 */
	public PK create(T newInstance) throws ServiceException;

	/**
	 * Reads the object previously added using specified id as
	 * primary key
	 * 
	 * @param key the primary key
	 * @return the entity
	 * @throws ServiceException 
	 */
	public T read(PK id) throws ServiceException;
	
	/**
	 * Saves modified state of object 
	 * 
	 * @param transientObject the entity to update
	 * @throws ServiceException 
	 */
	public void update(T updatedInstance) throws ServiceException;

	/**
	 * Deletes record about the object
	 * 
	 * @param persistentObject the entity to delete
	 * @throws ServiceException 
	 */
	public void delete(PK id) throws ServiceException;
}
