package com.epam.javalab.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.javalab.newsmanagement.dao.ICommentDao;
import com.epam.javalab.newsmanagement.dao.util.DAOUtil;
import com.epam.javalab.newsmanagement.entity.Comment;
import com.epam.javalab.newsmanagement.exception.DAOException;

/**
 * JDBC implementation of the <code>ICommentDao</code> using Oracle
 * <code>DataSource</code> connection pool and prepared queries
 * 
 * @author Dzmitry Rastsiusheuski
 *
 */
public class CommentDaoJDBCImpl implements ICommentDao {

	private final static String INSERT_COMMENT_SQL = "INSERT INTO COMMENTS "
			+ "(COMMENT_TEXT, CREATION_DATE, NEWS_ID) VALUES(?, ?, ?)";
	private final static String SELECT_COMMENT_BY_ID_SQL = "SELECT COMMENT_ID, COMMENT_TEXT, "
			+ "NEWS_ID, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";
	private final static String UPDATE_COMMENT_BY_ID_SQL = "UPDATE COMMENTS SET COMMENT_TEXT = ?,"
			+ "CREATION_DATE = ?, NEWS_ID = ? WHERE COMMENT_ID = ?";
	private final static String DELETE_COMMENT_BY_ID_SQL = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private final static String SELECT_ALL_COMMENTS_BY_NEWS_ID = "SELECT COMMENT_ID, COMMENT_TEXT, NEWS_ID,"
			+ "CREATION_DATE FROM COMMENTS WHERE NEWS_ID = ?";

	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/**
	 * Creates a new comment in database and returns generated primary key
	 * 
	 * @param newInstance the comment
	 * @return the primary key
	 * @throws DAOException if can't create a new comment
	 */
	@Override
	public Long create(Comment newInstance) throws DAOException {
		Long commentId = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet generatedKeys = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(INSERT_COMMENT_SQL,
					new String[] { "COMMENT_ID" });
			ps.setString(1, newInstance.getCommentText());
			ps.setTimestamp(2, new Timestamp(newInstance.getCreationDate()
					.getTime()));
			ps.setLong(3, newInstance.getNewsId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				generatedKeys = ps.getGeneratedKeys();
				if (null != generatedKeys && generatedKeys.next()) {
					commentId = generatedKeys.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DAOException("Can't create a comment", e);
		} finally {
			DAOUtil.close(generatedKeys, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return commentId;
	}

	/**
	 * Extracts the comment previously added to database using specified id as
	 * primary key
	 * 
	 * @param commentId the primary key
	 * @return the author
	 * @throws DAOException if there isn't such a comment in database
	 */
	@Override
	public Comment read(Long commentId) throws DAOException {
		Comment comment = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SELECT_COMMENT_BY_ID_SQL);
			ps.setLong(1, commentId);
			rs = ps.executeQuery();
			while (rs.next()) {
				comment = parseResultSet(rs);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't get comment by id", e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return comment;
	}

	/**
	 * Saves modified state of the comment in database
	 * 
	 * @param comment the comment to update
	 * @throws DAOException if there isn't such a comment or it has been updated more
	 * than one comment
	 */
	@Override
	public void update(Comment comment) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(UPDATE_COMMENT_BY_ID_SQL);
			ps.setString(1, comment.getCommentText());
			ps.setTimestamp(2, new Timestamp(comment.getCreationDate()
					.getTime()));
			ps.setLong(3, comment.getNewsId());
			ps.setLong(4, comment.getId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new DAOException(
						"Updated more or less than one comment by the query");
			}
		} catch (SQLException e) {
			throw new DAOException("Can't update comment by id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/**
	 * Deletes the record about the comment from database
	 * 
	 * @param commentId the comment id
	 * @throws DAOException if there isn't such a comment to delete or the query deleted
	 * more or less than one comment
	 */
	@Override
	public void delete(Long commentId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(DELETE_COMMENT_BY_ID_SQL);
			ps.setLong(1, commentId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new DAOException(
						"Deleted more than one comment by the query");
			}
		} catch (SQLException e) {
			throw new DAOException("Can't delete comment by id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/**
	 * Gets all comments associated with the news by news id
	 * 
	 * @param newsId the news id
	 * @return the list of news
	 * @throws DAOException if can't get all comments by news id
	 */
	@Override
	public List<Comment> getAllNewsComments(Long newsId) throws DAOException {
		List<Comment> comments = new ArrayList<Comment>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SELECT_ALL_COMMENTS_BY_NEWS_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Comment comment = parseResultSet(rs);
				comments.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't get all comments by news id", e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return comments;
	}

	/**
	 * Extracts the data returned by the query from the result set and pack it
	 * into the <code>Comment</code>
	 * 
	 * @param rs the result set of the executed query
	 * @return the comment entity
	 * @throws SQLException
	 */
	public Comment parseResultSet(ResultSet rs) throws SQLException {
		Comment comment = new Comment();
		comment.setId(rs.getLong("COMMENT_ID"));
		comment.setNewsId(rs.getLong("NEWS_ID"));
		comment.setCommentText(rs.getString("COMMENT_TEXT"));
		comment.setCreationDate(new Date(rs.getTimestamp("CREATION_DATE")
				.getTime()));
		return comment;
	}
	
}
