package com.epam.javalab.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.javalab.newsmanagement.dao.IAuthorDao;
import com.epam.javalab.newsmanagement.entity.Author;
import com.epam.javalab.newsmanagement.exception.DAOException;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.IAuthorService;

public class AuthorServiceImpl implements IAuthorService {

	private final static Logger logger = Logger
			.getLogger(AuthorServiceImpl.class);
	private IAuthorDao authorDao;

	public void setAuthorDao(IAuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	/**
	 * Creates new author and returns generated primary key
	 * 
	 * @param newInstance the author
	 * @return the primary key
	 * @throws ServiceException if can't create a new author
	 */
	@Override
	public Long create(Author newInstance) throws ServiceException {
		try {
			return authorDao.create(newInstance);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Reads the author previously added using specified id as
	 * primary key
	 * 
	 * @param key the primary key
	 * @return the author
	 * @throws ServiceException if there isn't such an author in database
	 */
	@Override
	public Author read(Long authorId) throws ServiceException {
		try {
			return authorDao.read(authorId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Saves modified state of the author
	 * 
	 * @param author the author to update
	 * @throws ServiceException if there isn't such an author or it has been updated more
	 * than one author
	 */
	@Override
	public void update(Author author) throws ServiceException {
		try {
			authorDao.update(author);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Deletes record about the author 
	 * 
	 * @param authorId author id
	 * @throws ServiceException if there isn't such an author to delete
	 */
	@Override
	public void delete(Long authorId) throws ServiceException {
		try {
			authorDao.delete(authorId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.IAuthorService#getNewsAuthor(java.lang.Long)
	 */
	@Override
	public Author getNewsAuthor(Long newsId) throws ServiceException {
		try {
			return authorDao.getNewsAuthor(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.service.IAuthorService#getAllAuthors()
	 */
	@Override
	public List<Author> getAllAuthors() throws ServiceException {
		try {
			return authorDao.getAllAuthors();
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

}
