package com.epam.javalab.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.javalab.newsmanagement.dao.IAuthorDao;
import com.epam.javalab.newsmanagement.dao.util.DAOUtil;
import com.epam.javalab.newsmanagement.entity.Author;
import com.epam.javalab.newsmanagement.exception.DAOException;
import com.epam.javalab.newsmanagement.exception.ServiceException;

/**
 * JDBC implementation of the <code>IAuthorDao</code> using Oracle
 * <code>DataSource</code> connection pool and prepared queries
 * 
 * @author Dzmitry Rastsiusheuski
 *
 */
public class AuthorDaoJDBCImpl implements IAuthorDao {

	private final static String INSERT_AUTHOR_SQL = "INSERT INTO AUTHOR (NAME) VALUES(?)";
	private final static String SELECT_AUTHOR_BY_ID_SQL = "SELECT AUTHOR_ID, NAME, EXPIRED_DATE FROM AUTHOR "
			+ "WHERE AUTHOR_ID = ?";
	private final static String UPDATE_AUTHOR_BY_ID_SQL = "UPDATE AUTHOR SET NAME = ? "
			+ "WHERE AUTHOR_ID = ?";
	private final static String EXPIRE_AUTHOR_BY_ID_SQL = "UPDATE AUTHOR SET EXPIRED_DATE = ?"
			+ "WHERE AUTHOR_ID = ?";
	private final static String SELECT_AUTHOR_BY_NEWS_ID_SQL = "SELECT AUTHOR.AUTHOR_ID, AUTHOR.NAME, AUTHOR.EXPIRED_DATE "
			+ "FROM AUTHOR JOIN NEWS_AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID "
			+ "WHERE NEWS_AUTHOR.NEWS_ID = ?";
	private final static String SELECT_ALL_AUTHORS_SQL = "SELECT AUTHOR_ID, NAME, EXPIRED_DATE FROM AUTHOR ORDER BY NAME NULLS LAST";
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/**
	 * Creates a new author in database and returns generated primary key
	 * 
	 * @param newInstance the author
	 * @return the primary key
	 * @throws DAOException if can't create a new author
	 */
	public Long create(Author newInstance) throws DAOException {
		Long authorId = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet generatedKeys = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(INSERT_AUTHOR_SQL,
					new String[] { "AUTHOR_ID" });
			ps.setString(1, newInstance.getName());
			int affectedRows = ps.executeUpdate();
			if (affectedRows > 0) {
				generatedKeys = ps.getGeneratedKeys();
				if (null != generatedKeys && generatedKeys.next()) {
					authorId = generatedKeys.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DAOException("Can't create an author", e);
		} finally {
			DAOUtil.close(generatedKeys, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authorId;
	}

	/**
	 * Extracts the author previously added to database using specified id as
	 * primary key
	 * 
	 * @param authorId the primary key
	 * @return the author
	 * @throws DAOException if there isn't such an author in database
	 */
	public Author read(Long authorId) throws DAOException {
		Author author = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SELECT_AUTHOR_BY_ID_SQL);
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			while (rs.next()) {
				author = parseResultSet(rs);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't read author by id ", e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return author;
	}

	/**
	 * Saves modified state of the author in database
	 * 
	 * @param author the author to update
	 * @throws DAOException if there isn't such an author or it has been updated more
	 * than one author
	 */
	public void update(Author author) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(UPDATE_AUTHOR_BY_ID_SQL);
			ps.setString(1, author.getName());
			ps.setLong(2, author.getId());
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new DAOException(
						"Updated more or less than one author by the query");
			}
		} catch (SQLException e) {
			throw new DAOException("Can't update author by id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/**
	 * Deletes record about the author from database
	 * 
	 * @param authorId the author id
	 * @throws DAOException if there isn't such an author to delete or the query deleted
	 * more or less than one row
	 */
	public void delete(Long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(EXPIRE_AUTHOR_BY_ID_SQL);
			ps.setTimestamp(1, new java.sql.Timestamp(new Date().getTime()));
			ps.setLong(2, authorId);
			int affectedRows = ps.executeUpdate();
			if (affectedRows != 1) {
				throw new DAOException(
						"Expired more or less than one author by the query");
			}
		} catch (SQLException e) {
			throw new DAOException("Can't expire author by id", e);
		} finally {
			DAOUtil.close(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	/**
	 * Gets the news author by news id
	 * 
	 * @param newsId the news id
	 * @return the author entity
	 * @throws ServiceException
	 */
	@Override
	public Author getNewsAuthor(Long newsId) throws DAOException {
		Author author = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SELECT_AUTHOR_BY_NEWS_ID_SQL);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				author = parseResultSet(rs);
			}
		} catch (SQLException e) {
			throw new DAOException("Can't get all news authors by news id ", e);
		} finally {
			DAOUtil.close(rs, ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return author;
	}

	/* (non-Javadoc)
	 * @see com.epam.javalab.newsmanagement.dao.IAuthorDao#getAllAuthors()
	 */
	public List<Author> getAllAuthors() throws DAOException {
		List<Author> authors = new ArrayList<Author>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			st = con.createStatement();
			rs = st.executeQuery(SELECT_ALL_AUTHORS_SQL);
			while (rs.next()) {
				Author author = parseResultSet(rs);
				authors.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException("Cant't get list of all authors", e);
		} finally {
			DAOUtil.close(rs, st);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authors;
	}

	/**
	 * Extracts the data returned by the query from the result set and pack it
	 * into the <code>Author</code>
	 * 
	 * @param rs the result set of the executed query
	 * @return the author entity
	 * @throws SQLException
	 */
	private Author parseResultSet(ResultSet rs) throws SQLException {
		Author author = new Author();
		author.setId(rs.getLong("AUTHOR_ID"));
		author.setName(rs.getString("NAME"));
		if (rs.getDate("EXPIRED_DATE") != null) {
			author.setExpiredDate(new Date(rs.getDate("EXPIRED_DATE")
					.getTime()));
		}
		return author;
	}
	
}
