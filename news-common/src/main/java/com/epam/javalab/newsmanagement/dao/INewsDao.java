package com.epam.javalab.newsmanagement.dao;

import java.util.List;

import com.epam.javalab.newsmanagement.entity.News;
import com.epam.javalab.newsmanagement.entity.SearchCriteria;
import com.epam.javalab.newsmanagement.exception.DAOException;
import com.epam.javalab.newsmanagement.exception.ServiceException;

/**
 * Provides additional methods to manage business logic which is associated with
 * News entity. Responsible for binding the news data. Attaches additional
 * information to the news.
 * 
 * @author Dzmitry_Rastsiusheuski
 * 
 */
public interface INewsDao extends IGenericDao<News, Long> {

	/**
	 * Deletes list of news using list of news identifiers
	 * 
	 * @param newsIdList list of news identifiers
	 * @throws DAOException
	 */
	public void deleteNewsList(Long[] newsIdList) throws DAOException;

	/**
	 * Attaches the author to the news using <code>newsId</code> and
	 * <code>authorId</code>
	 * 
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws DAOException if can't attach the author to 
	 * the news or it has benn attached more than one author 
	 * to the news
	 */
	public void attachNewsAuthor(Long newsId, Long authorId) throws DAOException;

	/**
	 * Adds specified tags to the news by the news id
	 * 
	 * @param newsId the news id
	 * @param tagIdList a tag list
	 * @throws DAOException
	 */
	public void attachNewsTags(Long newsId, Long[] tagIdList) throws DAOException;

	/**
	 * Returns the next news id that follow the passed news id.
	 * If SearchCriteria is not empty the query will be filtered in advance. 
	 * 
	 * @param id the news id
	 * @param searchCriteria the search criteria to look for the news
	 * @return the next news id
	 * @throws DAOException 
	 */
	public Long findNextNewsId(SearchCriteria searchCriteria, Long id)
			throws DAOException;
	
	/**
	 * Returns the previous news id that is located before the passed news id.
	 * If SearchCriteria is not empty the query will be filtered in advance. 
	 * 
	 * @param id the news id
	 * @param searchCriteria the search criteria to look for the news
	 * @return the previous news id
	 * @throws DAOException
	 */
	public Long findPreviousNewsId(SearchCriteria searchCriteria, Long id)
			throws DAOException;
	
	/**
	 * Detaches the news author from the news by news id
	 * 
	 * @param newsId the news id
	 * @throws DAOException if can't detach author from the news
	 */
	public void detachNewsAuthor(Long newsId) throws DAOException;
	
	/**
	 * Deletes all the tags from the news by news id
	 * 
	 * @param newsId the news id
	 * @param tagIds list of tag identifiers
	 * @throws DAOException if can't attach tags to the news
	 */
	public void detachNewsTags(Long newsId) throws DAOException;
	
	/**
	 * Returns total amount of rows according to the search criteria
	 * 
	 * @param searchCriteria the search criteria
	 * @return Total amount of rows
	 * @throws ServiceException If something fails at DAO level
	 */
	public int count(SearchCriteria sc) throws DAOException;

	/**
	 * Returns list of news according to the search criteria in specified range
	 * 
	 * @param from the number of the record
	 * @param offset count of news to get
	 * @param sc the search criteria
	 * @return list of news
	 * @throws DAOException
	 */
	public List<News> getLimitNews(int from, int offset, SearchCriteria sc) throws DAOException;

}
