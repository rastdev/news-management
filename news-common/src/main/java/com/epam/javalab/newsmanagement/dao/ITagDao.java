package com.epam.javalab.newsmanagement.dao;

import java.util.List;

import com.epam.javalab.newsmanagement.entity.Tag;
import com.epam.javalab.newsmanagement.exception.DAOException;
/**
 * Provides additional methods to manage business logic which is particular for
 * the <tt>TagTO</tt> object
 * 
 * @author Dzmitry Rastsiusheuski
 * 
 */
public interface ITagDao extends IGenericDao<Tag, Long> {
	/**
	 * Gets all tags associated with the news by news id
	 * 
	 * @param newsId the news id
	 * @return the list of tags
	 * @throws DAOException
	 */
	public List<Tag> getAllNewsTags(Long newsId) throws DAOException;
	

	/**
	 * Returns list of news tags 
	 * 
	 * @return list of tags
	 * @throws DAOException
	 */
	public List<Tag> getAllTags() throws DAOException;
}
