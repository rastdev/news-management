package com.epam.javalab.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.javalab.newsmanagement.dao.ITagDao;
import com.epam.javalab.newsmanagement.entity.Tag;
import com.epam.javalab.newsmanagement.exception.DAOException;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.ITagService;

public class TagServiceImpl implements ITagService {

	private final static Logger logger = Logger.getLogger(TagServiceImpl.class);
	private ITagDao tagDao;

	public void setTagDao(ITagDao tagDao) {
		this.tagDao = tagDao;
	}
	
	/**
	 * Creates a new tag and returns generated primary key
	 * 
	 * @param newInstance the tag
	 * @return the primary key
	 * @throws ServiceException if can't create a new tag
	 */
	@Override
	public Long create(Tag newInstance) throws ServiceException {
		try {
			return tagDao.create(newInstance);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Reads the tag previously added using specified id as
	 * primary key
	 * 
	 * @param tagId the primary key
	 * @return the tag
	 * @throws ServiceException if there isn't such a tag
	 */
	@Override
	public Tag read(Long tagId) throws ServiceException {
		try {
			return tagDao.read(tagId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Saves modified state of the tag
	 * 
	 * @param tag the tag to update
	 * @throws ServiceException if there isn't such a tag or it has been updated 
	 * more than one tag
	 */
	@Override
	public void update(Tag tag) throws ServiceException {
		try {
			tagDao.update(tag);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Deletes the record about the tag 
	 * 
	 * @param tag the tag id
	 * @throws ServiceException if there isn't such a tag to delete or
	 *  the query deleted more or less than one tag
	 */
	@Override
	public void delete(Long tagId) throws ServiceException {
		try {
			tagDao.delete(tagId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Gets all tags associated with the news by news id
	 * 
	 * @param newsId the news id
	 * @return the list of tags
	 * @throws ServiceException if can't get all news tags
	 */
	@Override
	public List<Tag> getAllNewsTags(Long newsId) throws ServiceException {
		try {
			return tagDao.getAllNewsTags(newsId);
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * Returns list of news tags 
	 * 
	 * @return list of tags
	 * @throws ServiceException
	 */
	@Override
	public List<Tag> getAllTags() throws ServiceException {
		try {
			return tagDao.getAllTags();
		} catch (DAOException e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

}
