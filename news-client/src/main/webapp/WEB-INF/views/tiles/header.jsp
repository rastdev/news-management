<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>

<tiles:getAsString name="title"/>
<div id="locale">
	<a class="btn" href="?lang=ru">RU</a>
	<a class="btn" href="?lang=en">EN</a>
</div>