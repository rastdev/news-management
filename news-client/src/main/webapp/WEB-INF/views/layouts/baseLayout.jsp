<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<tiles:importAttribute name="stylesheets"/>
<tiles:importAttribute name="javascript"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<c:forEach var="css" items="${stylesheets}">
	<link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
</c:forEach>

<c:forEach var="js" items="${javascript}">
	<script type="text/javascript" src="<c:url value="${js}"/>"></script>
</c:forEach>
<script type="text/javascript">

$(document).ready(function(){
	
	$('#target').submit(function(e) {
		var frm = $('#target');
		e.preventDefault();

	    var data = {}
	    var Form = this;

	    //Gather Data also remove undefined keys(buttons)
	    $.each(this, function(i, v){
	            var input = $(v);
	        data[input.attr("name")] = input.val();
	        delete data["undefined"];
	    });
    $.ajax({

        type: frm.attr('method'),
        url: frm.attr('action'),
        contentType: 'application/json',
        mimeType: 'application/json',
        data : JSON.stringify(data),
        success : function(callback){
        	$('#message').val('');
        	var container = document.getElementById('comment-container');
        	var comment = document.createElement("div");
        	comment.className = "comment";
        	
        	var commentDate = document.createElement("div");
         	commentDate.className = "comment-date";
         	commentDate.textContent = callback.creationDate;
         	comment.appendChild(commentDate);
         	
        	var commentMessage = document.createElement("div");
        	commentMessage.className = "message";
        	commentMessage.textContent = callback.commentText;
        	comment.appendChild(commentMessage);
        	
        	container.appendChild(comment);
        	
        	var absentComments = document.getElementById("absent-comments");
        	if (absentComments != null) {
        		absentComments.remove();
        	}
        	notify('<spring:message
					code="notification.comment.success" />', 'success');
        	
        },
        error : function(){
        	notify('<spring:message
					code="notification.comment.error" />', 'error');
        }
    });
	});

});
</script>
<title>Insert title here</title>
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<tiles:insertAttribute name="header"/>
		</div>
		<div id="middle">
			<div id="center">
				<div id="content">
					<tiles:insertAttribute name="body"/>
				</div>
			</div>
		</div>
		<div id="footer">
			<tiles:insertAttribute name="footer"/>
		</div>
	</div>
</html>