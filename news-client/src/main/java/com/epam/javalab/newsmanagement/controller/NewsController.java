package com.epam.javalab.newsmanagement.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javalab.newsmanagement.entity.Author;
import com.epam.javalab.newsmanagement.entity.Comment;
import com.epam.javalab.newsmanagement.entity.News;
import com.epam.javalab.newsmanagement.entity.NewsTO;
import com.epam.javalab.newsmanagement.entity.SearchCriteria;
import com.epam.javalab.newsmanagement.entity.Tag;
import com.epam.javalab.newsmanagement.exception.ResourceNotFoundException;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.IAuthorService;
import com.epam.javalab.newsmanagement.service.ICommentService;
import com.epam.javalab.newsmanagement.service.INewsManagementService;
import com.epam.javalab.newsmanagement.service.INewsService;
import com.epam.javalab.newsmanagement.service.ITagService;
import com.epam.javalab.newsmanagement.util.PageHelper;
import com.epam.javalab.newsmanagement.util.editor.LocalizedDateEditor;
import com.epam.javalab.newsmanagement.util.formatter.FormatHelper;

@Controller
public class NewsController {

	@Autowired
	private INewsManagementService newsManagementService;
	@Autowired
	private INewsService newsService;
	@Autowired
	private IAuthorService authorService;
	@Autowired
	private ITagService tagService;
	@Autowired
	private ICommentService commentService;
	@Autowired
	private FormatHelper formatHelper;

	public void setNewsManagementService(
			INewsManagementService newsManagementService) {
		this.newsManagementService = newsManagementService;
	}

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new LocalizedDateEditor(
				formatHelper));
	}
	
	/**
	 * Gets all the news accordingly to the number of page.
	 * Passes all the tags and authors to the model.
	 * 
	 * @param page number of the page
	 * @param request request to get session
	 * @return model and name of view to display
	 * @throws ServiceException
	 */
	@RequestMapping(value = { "/news", "/" })
	public ModelAndView showNews(
			@RequestParam(required = false, defaultValue = "1") String page,
			HttpServletRequest request) throws ServiceException {

		PageHelper pageHelper = new PageHelper();
		SearchCriteria sc = getSearchCriteriaFromSession(request);
		int totalRows = newsService.count(sc);
		pageHelper.calculate(Integer.parseInt(page), totalRows);

		List<NewsTO> newsList = newsManagementService.getLimitNews(
				pageHelper.getFirstRow(), pageHelper.getOffset(), sc);
		List<Author> authors = authorService.getAllAuthors();
		List<Tag> tags = tagService.getAllTags();

		ModelAndView modelAndView = new ModelAndView("newsList");
		modelAndView.addObject("authors", authors);
		modelAndView.addObject("tags", tags);
		modelAndView.addObject("newsTOList", newsList);
		modelAndView.addObject("searchCriteria", sc);
		modelAndView.addObject(pageHelper);
		return modelAndView;
	}

	/**
	 * Returns search criteria attribute from session. If there is no attribute
	 * then creates a new epmty search critera object.
	 * 
	 * @param request request to get session
	 * @return the search criteria
	 */
	private SearchCriteria getSearchCriteriaFromSession(
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		SearchCriteria sc = (SearchCriteria) session
				.getAttribute("searchCriteria");
		if (sc == null) {
			sc = new SearchCriteria();
			session.setAttribute("searchCriteria", sc);
		}
		return sc;
	}
	
	/**
	 * Creates a new search criteria accordingly to passed search parameters.
	 * Adds search criteria attribute to session. Redirects to show news page.
	 * 
	 * @param request request to get session
	 * @return model and page to redirect
	 */
	@RequestMapping(value = { "/news/filter" })
	public ModelAndView filterNews(@ModelAttribute("searchCriteria") SearchCriteria sc, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("searchCriteria", sc);

		ModelAndView modelAndView = new ModelAndView("redirect:/news");
		return modelAndView;
	}

	
	/**
	 * Resets search filter creating a new empty search criteria 
	 * and adds it to session. Redirects to show news page.
	 * 
	 * @param request
	 * @return model and page to redirect
	 */
	@RequestMapping(value = { "/news/resetFilter" }, method = RequestMethod.POST)
	public ModelAndView resetFilterNews(HttpServletRequest request) {
		HttpSession session = request.getSession();
		SearchCriteria sc = new SearchCriteria();
		session.setAttribute("searchCriteria", sc);
		
		ModelAndView modelAndView = new ModelAndView("redirect:/news");
		return modelAndView;
	}


	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleResourceNotFoundException() {
		return "error404";
	}

	@RequestMapping(value = "/news/{id}", method = RequestMethod.GET)
	public String viewNews(@PathVariable Long id,
			HttpServletRequest request, Model model) throws NumberFormatException,
			ServiceException {

		NewsTO newsTO = newsManagementService.readNews(id);
		News news = newsTO.getNews();
		if (news == null) {
			throw new ResourceNotFoundException();
		}
		SearchCriteria sc = getSearchCriteriaFromSession(request);
		Long nextNewsId = newsService.findNextNewsId(sc, id);
		Long previousNewsId = newsService.findPreviousNewsId(sc, id);
		Comment comment = new Comment();
		comment.setNewsId(news.getId());
		
		model.addAttribute("nextNewsId", nextNewsId);
		model.addAttribute("previousNewsId", previousNewsId);
		model.addAttribute("newsTO", newsTO);
		if (!model.containsAttribute("comment")) {
			model.addAttribute("comment", comment);
		}

		return "viewNews";
	}

}