package com.epam.javalab.newsmanagement.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.javalab.newsmanagement.entity.Comment;
import com.epam.javalab.newsmanagement.exception.ServiceException;
import com.epam.javalab.newsmanagement.service.ICommentService;

@Controller
@RequestMapping(value = "/news")
public class CommentController {

	@Autowired
	private ICommentService commentService;

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	@RequestMapping(value = "/{newsId}/comment/post", method = {
			RequestMethod.POST, RequestMethod.GET })
	public ModelAndView createComment(
			@Valid @ModelAttribute("comment") Comment comment,
			BindingResult result, @PathVariable Long newsId,
			RedirectAttributes redirectAttribute) throws ServiceException {

		if (result.hasErrors()) {
			redirectAttribute.addFlashAttribute(
					"org.springframework.validation.BindingResult.comment",
					result);
			redirectAttribute.addFlashAttribute("comment", comment);
			ModelAndView modelAndView = new ModelAndView("redirect:/news/"
					+ newsId);
			return modelAndView;
		}

		Date creationDate = new Date();
		comment.setCreationDate(creationDate);
		commentService.create(comment);

		ModelAndView modelAndView = new ModelAndView("redirect:/news/" + newsId);
		return modelAndView;
	}
}
